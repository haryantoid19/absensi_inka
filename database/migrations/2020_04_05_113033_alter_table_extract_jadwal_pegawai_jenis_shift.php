<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableExtractJadwalPegawaiJenisShift extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    private $tableName='extract_jadwal_pegawai';
    
    public function up()
    {
        $columnName='jenis_shift';
        if (Schema::hasTable($this->tableName)) {
            if (Schema::hasColumn($this->tableName, $columnName)) {

                Schema::table($this->tableName,function ($table) use ($columnName) {
                    $table->string($columnName)->change();
                });
                
            }
            
            return 1;
        }
        return 0;
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
