<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableHt45RfidClean extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    private $tableName='ht45_rfid_clean';

    public function up()
    {
        //
        if (!Schema::hasTable($this->tableName)) {
            Schema::create($this->tableName, function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->bigInteger('id_record')->nullable();
                $table->string('nip')->nullable();
               
                $table->string('rfid_epc')->nullable();
                $table->string('rfid_tid')->nullable();
                $table->string('rfid_userdata')->nullable();
               
                $table->string('rfid_reader_ip')->nullable();
                $table->string('rfid_reader_mac')->nullable();
                $table->integer('rfid_reader_ant')->nullable();
                $table->string('created_date')->nullable();
                $table->string('status')->nullable();
                $table->timestamps();
                $table->dateTime('deleted_at')->nullable();
                $table->dateTime('created_at_masuk')->nullable();
                $table->dateTime('created_at_pulang')->nullable();
                $table->date('tanggal')->nullable();
                $table->string('flag_update')->nullable();
                $table->string('flag_ht08')->nullable();
                
                $table->string('flag_ht45')->nullable();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
