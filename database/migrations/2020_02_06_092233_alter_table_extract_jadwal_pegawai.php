<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableExtractJadwalPegawai extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    private $tableName='extract_jadwal_pegawai';
    public function up()
    {
        $columnName='flag_upload';
        if (Schema::hasTable($this->tableName)) {
            if (Schema::hasColumn($this->tableName, $columnName)) {
                echo('Table '. $columnName.' already exist in '.$this->tableName);
                echo('============================');
                return 0;
            }
            echo('Alter Table '.$this->tableName);
            echo('============================');
            Schema::table($this->tableName, function (Blueprint $table) use ($columnName) {
            
            
                $table->integer($columnName)->nullable();
            
            });
            return 1;
        }
        return 0;
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
