<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    private $tableName='users';

    public function up()
    {
        if (!Schema::hasTable($this->tableName)) {
            Schema::create($this->tableName, function (Blueprint $table) {
                $table->string('nip');
                $table->string('password')->nullable();
                $table->string('kode_otp_ganti_password')->nullable();
                $table->string('kode_otp_reset_password')->nullable();
                $table->string('token_firebase')->nullable();
                $table->string('token_login_mobile')->nullable();
                $table->dateTime('token_login_mobile_kadaluarsa')->nullable();
                $table->rememberToken();
                $table->tinyInteger('has_changed_password')->nullable();
                $table->integer('total_poin_asli')->nullable();
                $table->integer('total_poin_aligment')->nullable();
                $table->dateTime('created_at')->nullable();
                $table->dateTime('updated_at')->nullable();
                $table->dateTime('deleted_at')->nullable();
                $table->string('foto')->nullable();
                $table->string('rfid_tid')->nullable();
                $table->string('versi_apk')->nullable();
                $table->string('jenis_os')->nullable();
                $table->primary('nip');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
