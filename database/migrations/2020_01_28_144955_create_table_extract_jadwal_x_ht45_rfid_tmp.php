<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableExtractJadwalXHt45RfidTmp extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    private $tableName='extract_jadwal_pegawai_x_ht45_rfid_tmp';
    public function up()
    {
        //
        if (!Schema::hasTable($this->tableName)) {
            Schema::create($this->tableName, function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->bigInteger('id_extract_jadwal')->nullable();
                $table->bigInteger('id_ht45_rfid_tmp')->nullable();
                $table->date('tanggal_absen')->nullable();
                $table->string('nip')->nullable();
                $table->integer('status_pulang_masuk')->nullable();
                $table->dateTime('date_time_detected')->nullable();
                $table->dateTime('created_at_rfid_tmp')->nullable();
                $table->string('rfid_tid')->nullable();
                $table->string('rfid_reader_mac')->nullable();
                $table->string('rfid_reader_ip')->nullable();
                $table->integer('flag_notif')->nullable();
                $table->integer('flag_upload')->nullable();
                $table->dateTime('created_at')->nullable();
                $table->dateTime('updated_at')->nullable();
                $table->dateTime('deleted_at')->nullable();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
