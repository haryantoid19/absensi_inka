<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableMasterRfidReader extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    private $tableName='master_rfid_reader';
    public function up()
    {
        //
        if (!Schema::hasTable($this->tableName)) {
            Schema::create($this->tableName, function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->string('rfid_reader_mac')->nullable();
                $table->string('rfid_reader_ip')->nullable();
                $table->string('nama')->nullable();
                $table->integer('worker')->nullable();
                $table->dateTime('created_at')->nullable();
                $table->dateTime('updated_at')->nullable();
                $table->dateTime('deleted_at')->nullable();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
