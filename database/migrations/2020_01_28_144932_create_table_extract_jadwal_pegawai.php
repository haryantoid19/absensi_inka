<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableExtractJadwalPegawai extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    private $tableName='extract_jadwal_pegawai';
    public function up()
    {
        //
        if (!Schema::hasTable($this->tableName)) {
            Schema::create($this->tableName, function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->date('tanggal_absen')->nullable();
                $table->string('nip')->nullable();
                $table->string('schtype')->nullable();
                $table->string('status')->nullable();
                $table->string('status_ht08')->nullable();
                $table->integer('jenis_shift')->nullable();
                $table->dateTime('range_start')->nullable();
                $table->dateTime('range_end')->nullable();
                $table->dateTime('date_time_detected')->nullable();
                $table->integer('flag_upload')->nullable();
                $table->dateTime('created_at')->nullable();
                $table->dateTime('updated_at')->nullable();
                $table->dateTime('deleted_at')->nullable();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
