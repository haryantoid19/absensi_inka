<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableHt08Keshc extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    private $tableName='ht08_keshc';

    public function up()
    {
        if (!Schema::hasTable($this->tableName)) {
            Schema::create($this->tableName, function (Blueprint $table) {
                $table->bigIncrements('id');

                $table->string("SCHTYPE")->nullable();
                $table->string("WORKGRPCODE")->nullable();
                $table->string("REGNO")->nullable();
                $table->dateTime("ABSDATE")->nullable();
                $table->integer("PERIODE")->nullable();
                $table->integer("BULAN")->nullable();
                $table->string("SUBNO")->nullable();
                $table->string("ATTDSTAT")->nullable();
                $table->string("PERMITNO")->nullable();
                $table->string("OVERTIMENO")->nullable();
                $table->integer("FURLOUGHCODE")->nullable();
                $table->string("FPERIODE")->nullable();
                $table->string("FURLOUGHNO")->nullable();
                $table->dateTime("DATEIN")->nullable();
                $table->dateTime("DATEOUT")->nullable();
                $table->dateTime("TIMEIN")->nullable();
                $table->dateTime("TIMEOUT")->nullable();
                $table->string("STATUS")->nullable();
                $table->string("REMARK")->nullable();
                $table->string("SPECIALSCH")->nullable();
                $table->string("SPDNO")->nullable();
                $table->dateTime("SCHIN")->nullable();
                $table->dateTime("SCHOUT")->nullable();
                $table->float("OVERHOUR", 8, 2)->nullable();
                $table->float("OVERCOUNT", 8, 2)->nullable();
                $table->string("PERMITST")->nullable();
                $table->integer("JAHIL")->nullable();
                $table->integer("JALEB")->nullable();
                $table->integer("MEHIL")->nullable();
                $table->string("ABCD")->nullable();
                
                $table->dateTime('created_at')->nullable();
                $table->dateTime('updated_at')->nullable();
                $table->dateTime('deleted_at')->nullable();
            });
        }
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
