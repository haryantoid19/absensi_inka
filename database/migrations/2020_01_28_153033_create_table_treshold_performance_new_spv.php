<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableTresholdPerformanceNewSpv extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    private $tableName='treshold_performance_new_spv';
    public function up()
    {
        //
        if (!Schema::hasTable($this->tableName)) {
            Schema::create($this->tableName, function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->date('date')->nullable();
                $table->dateTime('start')->nullable();
                $table->dateTime('stop')->nullable();
                
                $table->integer('data')->nullable();
                $table->integer('nip')->nullable();
                $table->integer('worker')->nullable();
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
