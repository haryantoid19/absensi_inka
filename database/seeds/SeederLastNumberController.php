<?php

use Illuminate\Database\Seeder;
use App\LastNumberController;

class SeederLastNumberController extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->seed();
    }
    public function seed(){
        $arrSeeder= $this->getSeeder();
        foreach ($arrSeeder as $key => $value) {
            $createData=LastNumberController::create($value);
        }
    }

    public function getSeeder(){
        $arrSeeder=[
            [
                'nama'                 =>'data absen finger masuk dan pulang',
                'last_number'          =>1738270,
                'deskripsi'            =>'nomor terakhir data ht45_keshc dengan nomesin 990 untuk case masuk dan pulang (0 dan 1)',
            ],

        ];

        return $arrSeeder;

    }
    
}
