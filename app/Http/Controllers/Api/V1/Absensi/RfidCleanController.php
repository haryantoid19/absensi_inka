<?php

namespace App\Http\Controllers\Api\V1\Absensi;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use DB;
use DateTime;
use App\User;
use App\Rfid;
use App\RfidTmp;
use App\RfidClean;
use stdClass;
use Log;
use App\MasterRfidReader;
use App\HT08KESHC;
use App\HT45KESHC;
use App\HT08MirroringCron;
use DateInterval;
use App\TresholdPerformanceNewSpv;
use App\MasterTresholdDateNewSpv;
use App\ExtractJadwalPegawai;
use App\Ht45RfidXExtractJadwal;
use App\TresholdPerformanceSpvUpload;

class RfidCleanController extends Controller
{
    public function index(Request $request){
        date_default_timezone_set("Asia/Bangkok");
        $now=new DateTime();
        $bulan=$request->bulan ? $request->bulan : $now->format('m');
        $tahun=$request->tahun ? $request->tahun : $now->format('Y');
        
        
        try {
            $user=User::where('nip',$request->nip)->first();
        } catch (Exception $e) {
            $user=NULL;
        }
        
        $arrSelect=[
            'nip',
            'created_at_masuk as waktu_deteksi_masuk',
            'created_at_pulang as waktu_deteksi_pulang',
            'rfid_tid as rfid_card',
            'status',
            'rfid_userdata as rfid_data',
            'rfid_reader_mac',
            'rfid_reader_ip',
            'tanggal',

        ];
        $parseQuery=[
            'nip'=>$request->nip,
            'user'=>$user,  
        ];
        $dataClean=RfidClean::with(['masterRfidReader'])->select($arrSelect);
        $dataClean=$dataClean->where(function($query) use ($parseQuery){
            $query->where('nip',$parseQuery['nip']);
            if($parseQuery['user']){
                $query->orWhere('rfid_tid',$parseQuery['user']->rfid_tid);
            }
        });
        $dataClean=$dataClean->whereMonth('tanggal',$bulan)->whereYear('tanggal',$tahun);

        $dataClean=$dataClean->orderBy('tanggal')->get();
        
        

        $groupByTanggalDataClean=$dataClean->groupBy('tanggal');

        $arrReturn=[];
        foreach($groupByTanggalDataClean as $key => $item){
            $returnClean                        = new stdClass;
            $returnClean->rfid_card             = '';
            $returnClean->rfid_data             = '';
            $returnClean->reader_name           = '';
            $returnClean->tanggal               = '';
            $returnClean->nip                   = ''; 
            $returnClean->waktu_deteksi_masuk   = '';
            $returnClean->waktu_deteksi_pulang  = '';
            
            $item=$item->sortBy('status');
            $item->each(function($childItem,$childKey) use ($returnClean){
              
                $returnClean->rfid_card             = $childItem->rfid_card;
                $returnClean->rfid_data             = $childItem->rfid_data ? $childItem->rfid_data : $returnClean->rfid_data  ;
                $returnClean->reader_name           = $childItem->masterRfidReader ? $childItem->masterRfidReader->nama : $childItem->rfid_reader_mac;
                $returnClean->tanggal               = $childItem->tanggal.' '.'00:00:00';
                $returnClean->nip                   = $childItem->nip;
                
                if($childItem->waktu_deteksi_masuk){
                    $returnClean->waktu_deteksi_masuk = $childItem->waktu_deteksi_masuk;
                }
                if($childItem->waktu_deteksi_pulang){
                    $returnClean->waktu_deteksi_pulang = $childItem->waktu_deteksi_pulang;
                }

               
                 
                
               
            });
            
            array_push($arrReturn,$returnClean);
           
        };

        return response()->json($arrReturn);
       
        
        

    }
}
