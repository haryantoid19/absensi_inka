<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use GuzzleHttp\Psr7\Request as GuzzleRequest;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ServerException;
use Carbon\Carbon;
use DB;
use DateTime;
use App\User;
use App\Rfid;
use App\RfidTmp;
use App\RfidClean;
use stdClass;
use Log;
use App\MasterRfidReader;
use App\HT08KESHC;
use App\HT45KESHC;
use App\HT08MirroringCron;
use DateInterval;
use App\TresholdPerformanceNewSpv;
use App\MasterTresholdDateNewSpv;
use App\ExtractJadwalPegawai;
use App\Ht45RfidXExtractJadwal;
use App\TresholdPerformanceSpvUpload;

class TresholdingAbsensi extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'absensi:tresholding {--worker=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Absensi Tresholding';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->configInsertHT45 = '1';
        $this->configUpdateHT08 = '1';
        // $this->configNotif = '0';
        $this->errorIndexTimeStamp = 0;
        
        $this->status_libur = ['L','R','B','C','S','I','K','D'];
        $this->status_aktif = [null,'',' ','Y','W','V','G','M','J','T','P','U','X'];
        $this->client=new Client();
        $this->url= env('EOFFICE_PRD_URL', 'localhost').'api/v1/';
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $spv=$this->spvTresholding();

    }
    public function spvTresholding(){
        $user=User::select('rfid_tid','nip')->whereNotNull('rfid_tid')->get();
        //DB::table('ht45_rfid_tmp_new_spv')->whereNotIn('rfid_tid',$user->pluck('rfid_tid'))->update(['flag_read_s1'=>'1']);
        //DB::table('ht45_rfid')->whereNotIn('rfid_tid',$user->pluck('rfid_tid'))->update(['flag_read_s1'=>'1']);
        
        date_default_timezone_set("Asia/Bangkok");
        $worker=$this->option('worker');
        $today = Carbon::now()->format('Y-m-d');
        try{
            $getDateProcess = MasterTresholdDateNewSpv::query()->where('id',$worker)->first();
            print_r($getDateProcess);
            // print_r('$getDateProcess');
        } catch(\Exception $e){
            Log::critical($e->getMessage());
            print_r($e->getMessage());
            sleep(3);
            
        }
        
        //$this->handleFraudRfidReaderMac($getDateProcess->date_process);
        $dateProcess = $getDateProcess->date_process;
        $runToday = $dateProcess;
        $from = date($runToday.' 00:00:00'); 
        $to = date($runToday.' 23:59:59');

        // if($runToday == '2018-11-18') : exit;endif;
        
        $jumlahData = 0;
        $jumlahNIP = 0;
        $nowStart = Carbon::now()->format('Y-m-d H:i:s');
        $this->handleJadwalStatusStringKosong();
        
        $masterRfidReader=MasterRfidReader::where('worker',$worker)->get();
        //dd($masterRfidReader->pluck('rfid_reader_mac'));

        $flagExclude = [
            '1',
            '2',
            '3',
            '4',
            '9',
            '99',//tidak nemu di extract jadwal
            '91',//di luar pulang atau masuk
            '92',//status non aktif
            '100',
            '101',
            '1001',
            '1002',
            '1003',
            '1004',
            '1009',

        ];
        /*kodingan lama */
        //$dataDB = DB::table('ht45_rfid_tmp_new_spv')->whereIn('rfid_reader_mac',$masterRfidReader
        $dataDB = RfidTmp::whereIn('rfid_reader_mac',$masterRfidReader
        ->pluck('rfid_reader_mac'))
        ->whereDate('created_at',$dateProcess)
        ->where( function($query) use ($flagExclude){
            $query->whereNull('flag_read_s1')
            ->orWhereNotIn('flag_read_s1', $flagExclude);
            //->orWhereNull('nip');
            
        })
        ->whereIn('rfid_tid',$user->pluck('rfid_tid'))
        ->orderByRaw('id ASC')
        ->limit(100)
        ->get();
        print_r($dataDB->pluck('rfid_tid'));
        print_r($dataDB);

        if(sizeof($dataDB)>0){ // (1)
           print_r('');
           print_r('[SPV] - $runToday = '.$runToday.' || $from = '.$from.' || $to = '.$to);
           print_r('[SPV-START] - '.$nowStart);
           print_r('SUPERVISOR TRESHOLDING RFID : ON');
           print_r("[SPV] - Jumlah data : ".count($dataDB));
            $jumlahData = count($dataDB);
            //dd($jumlahData);
            $arrRecordId = [];
            $arrTID = [];
            
            
            
            
           print_r("[SPV] - Jumlah TID : ".count($arrTID).'\n');
            /*kodingan lama */
            
            /*optimasi->>amaan*/

            
            $arrUniqueNIP   = $dataDB->unique('rfid_tid');
            $arrNipTid= $user->pluck('nip','rfid_tid');
            //dd($arrNipTid);

            
            
            

           print_r("[SPV] - JUMLAH NIP DARI TID : ".sizeof($arrUniqueNIP).'\n');
            $jumlahNIP = sizeof($arrUniqueNIP);
            
            $jum = 0;
            $statusLibur = ['L','R','B','C','S','I','K','D'];
            $statusAktif = [null,'',' ','Y','W','V','G','M','J','T','P','U','X'];
            $arrIdKotor=[];
            foreach($dataDB as $getData) // (1C)
            {
                $jum++;
                $paramDB = array(
                    'id_record' => $getData->id,
                    'rfid_tid' => $getData->rfid_tid,
                    'rfid_userdata' => $getData->rfid_userdata,
                    'rfid_reader_ip' => $getData->rfid_reader_ip,
                    'rfid_reader_mac' => $getData->rfid_reader_mac,
                    'rfid_reader_ant' => $getData->rfid_reader_ant,
                    'created_at' => $getData->created_at,
                );
                
                if(!empty($getData->rfid_tid)) // (1Ca)
                {
                    $getDateTimeDetected = new DateTime($getData->created_at);
                    $getDateDetected = $getDateTimeDetected->format('Y-m-d');
                    $getTimeDetected = $getDateTimeDetected->format('H:i:s');
                    print_r('a) Date Detected : '.$getDateDetected.' | Time Detected : '.$getTimeDetected.'\n');
                    $arrResultAbsen = [];
                    //$nip = $this->get_nip_fromTID($getData->rfid_tid); // [f3]
                    $nip=isset($arrNipTid[$getData->rfid_tid]) ? $arrNipTid[$getData->rfid_tid] : NULL;
                    //dd($nip);
                    if(!empty($nip)) // (1Ca-1)
                    {
                        //isi nip di kotor dan tmp
                        $get_id_kotor = DB::table('ht45_rfid')
                            ->where('rfid_tid', $getData->rfid_tid)
                            ->where('created_at', $getData->created_at)
                            ->first();
                        $id_kotor=$getData->id;                                    
                        if($get_id_kotor){
                            array_push($arrIdKotor,$get_id_kotor->id);
                            $id_kotor = $get_id_kotor->id;
                        }
                        
                        
                        
                        Rfid::where('id', $id_kotor)->update(['nip' => $nip]);
                        RfidTmp::where('id', $getData->id)->update(['nip' => $nip]);

                        $jadwal=ExtractJadwalPegawai::where('nip',$nip)
                        ->where(function($query) use ($getData){
                            $query->where('range_start','<=', $getData->created_at);
                            $query->where('range_end','>=', $getData->created_at);

                        })->first();
                       
                        if($jadwal){
                            /*fungsi insert extract jadwal*/
                            $arrInsertExtractJadwal=[
                                'id_extract_jadwal'=>$jadwal->id,
                                'id_ht45_rfid_tmp'=>$getData->id,
                                'tanggal_absen'=>$jadwal->tanggal_absen,
                                'nip'=>$jadwal->nip,
                                'status_pulang_masuk'=>$jadwal->status,
                                'date_time_detected'=>$jadwal->date_time_detected,
                                'created_at_rfid_tmp'=>$getData->created_at,
                                'rfid_tid'=>$getData->rfid_tid,
                                'rfid_reader_mac'=>$getData->rfid_reader_mac,
                                'rfid_reader_ip'=>$getData->rfid_reader_ip,

                            ];
                                

                            
                            if(in_array($jadwal->status_ht08,$statusLibur)){
                                RfidTmp::where('id', $getData->id)->update(['flag_read_s1' => '92']);
                                continue;
                                //dd('status_libur'.$jadwal->status_ht08);
                            }
                            else{
                                //$createHt45XExtractJadwal=$this->createHt45RfidXExtractJadwal( $arrInsertExtractJadwal);
                                if($jadwal->status=='0' || $jadwal->status==0){
                                    $dateTimeProcessed = new DateTime($getData->created_at);
                                    if(is_null($jadwal->date_time_detected)){
                                        $createHt45XExtractJadwal=$this->createHt45RfidXExtractJadwal($arrInsertExtractJadwal);
                                       
                                        $jadwal->date_time_detected=$getData->created_at;
                                        $jadwal->flag_upload=0;
                                        $jadwal->save();
                                        $dateTimeProcessed = new DateTime($getData->created_at);
                                    }
                                    else{
                                        
                                        $dateTimeJadwal= new DateTime($jadwal->date_time_detected);
                                        $dateTimeRfidTmp= new DateTime($getData->created_at);
                                        $dateTimeProcessed = new DateTime($jadwal->date_time_detected);
                                        if($dateTimeRfidTmp < $dateTimeJadwal){
                                            $createHt45XExtractJadwal=$this->createHt45RfidXExtractJadwal( $arrInsertExtractJadwal);
                                           
                                            $jadwal->date_time_detected=$getData->created_at;
                                            $jadwal->flag_upload=0;
                                            $jadwal->save();
                                            $dateTimeProcessed = new DateTime($getData->created_at);
                                        }
                                        else{
                                            $arrUpdateDataRfid=[
                                                'nip' => $nip,
                                                'flag_read_s1' => '100',
                                                'is_libur'  => '0',
                                                'is_ht45'  => '0'
                                            ];
                                            $updateRFIDTmp = RfidTmp::where('id', $getData->id)->update($arrUpdateDataRfid);
                                            continue;
                                            
                                        }
                                    }
                                    /*proses upload absensi */
                                    // $arrParse['REGNO']            = $nip;
                                    // $arrParse['ABSDATE']          = $jadwal->tanggal_absen;
                                    // $arrParse['datetimedetected'] = $dateTimeProcessed->format('Y-m-d H:i:s');
                                    // $arrParse["kolom"]            = 'timein';
                                    // $arrParse['kodeinout']        = 0;
                                    // $arrParse['nomesin']          = 999;
                                    // $ht08_in      = $this->updateHt08Lokal($jadwal->tanggal_absen,$nip, 'timein', $dateTimeProcessed->format('Y-m-d H:i:s'), $id_kotor);
                                    //$inputHt08Prd = $this->injectHt08Prd($arrParse);
                                    
                                    // $arrClean['id']= $id_kotor;
                                    // $arrClean['nip']=$nip;
                                    // $arrClean['rfid_epc']=$getData->rfid_epc;
                                    // $arrClean['rfid_tid']=$getData->rfid_tid;
                                    // $arrClean['rfid_userdata']=$getData->rfid_userdata;
                                    // $arrClean['rfid_reader_ip']=$getData->rfid_reader_ip;
                                    // $arrClean['rfid_reader_mac']=$getData->rfid_reader_mac;
                                    // $arrClean['rfid_reader_ant']=$getData->rfid_reader_ant;
                                    // $insertRfidClean=$this->insertClean($arrClean,'masuk',$dateTimeProcessed->format('Y-m-d H:i:s'),NULL,$jadwal->tanggal_absen);
                                    
                                    
                                    //print_r('$insertRfidClean = '.$insertRfidClean);
                                    // $insert_ht45 = $this->insertHt45Lokal($getData->id, $id_kotor, $nip, '0', $dateTimeProcessed->format('Y-m-d H:i:s'));
                                    //$inputHt45Prd = $this->injectHt45Prd($arrParse);

                                    /*proses upload absensi*/

                                    //tambah fungsi ht45 lokal
                                    $arrUpdateDataRfid=[
                                        'nip' => $nip,
                                        'flag_read_s1' => '1',
                                        'is_libur'  => '0',
                                        'is_ht45'  => '0'
                                    ];
                                    //$updateRFIDKotor = Rfid::where('id', $id_kotor)->update($arrUpdateDataRfid);
                                    
                                    $updateRFIDTmp = RfidTmp::where('id', $getData->id)->update($arrUpdateDataRfid);

                                }
                                elseif($jadwal->status=='1' || $jadwal->status==1){
                                    $dateTimeProcessed = new DateTime($getData->created_at);
                                    if(is_null($jadwal->date_time_detected)){
                                        $createHt45XExtractJadwal=$this->createHt45RfidXExtractJadwal( $arrInsertExtractJadwal);
                                       
                                        $jadwal->date_time_detected=$getData->created_at;
                                        $jadwal->flag_upload=0;
                                        $jadwal->save();
                                        $dateTimeProcessed = new DateTime($getData->created_at);
                                    }
                                    else{
                                        
                                        $dateTimeJadwal= new DateTime($jadwal->date_time_detected);
                                        $dateTimeRfidTmp= new DateTime($getData->created_at);
                                        if($dateTimeRfidTmp > $dateTimeJadwal){
                                            $createHt45XExtractJadwal=$this->createHt45RfidXExtractJadwal( $arrInsertExtractJadwal);
                                           
                                            $jadwal->date_time_detected=$getData->created_at;
                                            $jadwal->flag_upload=0;
                                            $jadwal->save();
                                            $dateTimeProcessed = new DateTime($getData->created_at);
                                        }
                                        else{
                                            $arrUpdateDataRfid=[
                                                'nip' => $nip,
                                                'flag_read_s1' => '101',
                                                'is_libur'  => '0',
                                                'is_ht45'  => '0'
                                            ];
                                            $updateRFIDTmp = RfidTmp::where('id', $getData->id)->update($arrUpdateDataRfid);
                                            continue;
                                            
                                        }
                                    }
                                    /*proses upload absensi */
                                        // $arrParse['REGNO']            = $nip;
                                        // $arrParse['ABSDATE']          = $jadwal->tanggal_absen;
                                        // $arrParse['datetimedetected'] = $dateTimeProcessed->format('Y-m-d H:i:s');
                                        // $arrParse["kolom"]            = 'timeout';
                                        // $arrParse['kodeinout']        = 1;
                                        // $arrParse['nomesin']          = 999;
                                        // $ht08_in      = $this->updateHt08Lokal($jadwal->tanggal_absen,$nip, 'timeout', $dateTimeProcessed->format('Y-m-d H:i:s'), $id_kotor);
                                        //$inputHt08Prd = $this->injectHt08Prd($arrParse);
                                        
                                        // $arrClean['id']= $id_kotor;
                                        // $arrClean['nip']=$nip;
                                        // $arrClean['rfid_epc']=$getData->rfid_epc;
                                        // $arrClean['rfid_tid']=$getData->rfid_tid;
                                        // $arrClean['rfid_userdata']=$getData->rfid_userdata;
                                        // $arrClean['rfid_reader_ip']=$getData->rfid_reader_ip;
                                        // $arrClean['rfid_reader_mac']=$getData->rfid_reader_mac;
                                        // $arrClean['rfid_reader_ant']=$getData->rfid_reader_ant;
                                        // $insertRfidClean=$this->insertClean($arrClean,'pulang',NULL,$dateTimeProcessed->format('Y-m-d H:i:s'),$jadwal->tanggal_absen);
                                        
                                        
                                        //print_r('$insertRfidClean = '.$insertRfidClean);
                                        // $insert_ht45 = $this->insertHt45Lokal($getData->id, $id_kotor, $nip, '1', $dateTimeProcessed->format('Y-m-d H:i:s'));
                                        //$inputHt45Prd = $this->injectHt45Prd($arrParse);

                                        /*proses upload absensi*/
                                        //tambah fungsi ht45 lokal
                                        $arrUpdateDataRfid=[
                                            'nip' => $nip,
                                            'flag_read_s1' => '1',
                                            'is_libur'  => '0',
                                            'is_ht45'  => '0'
                                        ];
                                        //$updateRFIDKotor = Rfid::where('id', $id_kotor)->update($arrUpdateDataRfid);
                                        
                                        $updateRFIDTmp = RfidTmp::where('id', $getData->id)->update($arrUpdateDataRfid);
                                    
                                }
                                else{
                                    RfidTmp::where('id', $getData->id)->update(['flag_read_s1' => '91']);
                                    continue;
                                    
                                }
                            }
                        }
                        else{
                            RfidTmp::where('id', $getData->id)->update(['flag_read_s1' => '99']);
                        }


                        
                        
                    } else { // if(!empty($nip))    // end (1Ca-1)
                       print_r('NIP kosong. Id Record = '.$paramDB['id_record'].'\n');
                        //Rfid::where('id', $paramDB['id_record'])->update(['flag_read_s1' => '1']);
                        RfidTmp::where('id', $getData->id)->update(['flag_read_s1' => '1']);
                        // RfidTmp::where('id', $paramDB['id_record'])->delete();
                    } 
                } else { // if(!empty($getData->rfid_tid))      // end (1Ca)
                    //Rfid::where('id', $paramDB['id_record'])->update(['flag_read_s1' => '1']);
                    RfidTmp::where('id', $getData->id)->update(['flag_read_s1' => '1']);
                    // RfidTmp::where('id', $paramDB['id_record'])->delete();
                }
                $this->errorIndexTimeStamp = 0; // (1Cb)
            }
            $nowStop = Carbon::now()->format('Y-m-d H:i:s');
            print_r('[SPV-STOP] - '.$nowStop.'\n');
            print_r('[SPV-START-STOP] - '.$nowStart.' -> '.$nowStop);
            if($jumlahData != 0) : // (1D)
                $paramReport['date'] = $runToday;
                $paramReport['start'] = $nowStart;
                $paramReport['stop'] = $nowStop;
                $paramReport['data'] = $jumlahData;
                $paramReport['nip'] = $jumlahNIP;
                $paramReport['worker'] = $worker;
                //$insertPerfomance = DB::table('treshold_performance')->insert($paramReport); // (1Da)
                $insertPerfomance=TresholdPerformanceNewSpv::create($paramReport);
                if($insertPerfomance) :print_r('INSERT PERFORMANCE SUKSES \n');endif;
                
                // $dataFraud = $this->insertToFraudRiwayatNull($arrIdKotor);
                // $this->spv_fraud->processingAbsensiFraudWithoutCommandWithParse($dataFraud);
            endif;
        }else{ // if(count($dataDB))
            //Log::info('SUPERVISOR TRESHOLDING RFID : OFF');
            if(Carbon::now()->format('Y-m-d')!= $dateProcess){ // (1-end)
                //Log::info('Update next date');
                $existDataAll = '0';
                print_r ("UPDATE Treshold Date ke HARI BERIKUTNYA \n");
                $date2= new Carbon($dateProcess);

                MasterTresholdDateNewSpv::query()->where('id', $worker)->update(['next' => '1',
                'date_process'=>$date2->addDay()->format('Y-m-d')]);
            }
        }
    }
    public function getMultiNipByTid_($arrayTID){
        $nowStart = Carbon::now()->format('Y-m-d H:i:s');
       print_r('[SPV] - START getMultiNipByTid_() : '.$nowStart);
        
        
        //$tidClean = array_unique($tidKotor);
        //print_r($tidClean);
        
        $nipTid=[];

        $user = DB::table('users')->select('nip')->whereIn('rfid_tid', $arrayTID)->get()->pluck('nip');
        
      
        $nowStop = Carbon::now()->format('Y-m-d H:i:s');
       print_r('[SPV] - STOP getMultiNipByTid_() : '.$nowStop);
        return $user;
    }

    public function get_multiple_schedule_local($dateDetected, $arrayNIP){
        date_default_timezone_set("Asia/Bangkok");
        $id=$arrayNIP;
        $idCount = count($id)-1;
        $counter = 0;
        $date = $dateDetected;
        $getDateTimeDetected = new DateTime($date);
        $getDateDetected = $getDateTimeDetected->format('Y-m-d');
        $newDateOver = date_create($getDateDetected);
        date_add($newDateOver,date_interval_create_from_date_string("-1 days"));
        $newDateOver = date_format($newDateOver,"Y-m-d");
        $newDateOver = $newDateOver.' 00:00:00.000';
        $ResultSet = [];
        while($counter <= $idCount ){
            $getResult = DB::table('ht08_mirroring_cron')->select(
                'SCHTYPE',
                'REGNO',
                'ABSDATE',
                'PERIODE',
                'BULAN',
                'DATEIN',
                'DATEOUT',
                'STATUS',
                'SCHIN',
                'SCHOUT'
            )->where('REGNO','=',$id[$counter]);
            $getResult = $getResult->where('ABSDATE', $date);
            $getResult = $getResult->get();
            //ubah status ke 9 jika tidak menemukan jadwal
            if(sizeof($getResult)< 1 ){
                Rfid::query()
                ->where('nip', $id[$counter])
                ->whereDate('created_at',$date)->update([
                    'flag_read_s1' => '9',
                    
                ]);
                RfidTmp::query()
                ->where('nip', $id[$counter])
                ->whereDate('created_at',$date)->update([
                    'flag_read_s1' => '9']);
            }
            //
            foreach($getResult as $key => $value) :
                $ResultSet[$value->REGNO][0] = $getResult;
            endforeach;

            $getResultKemarin = DB::table('ht08_mirroring_cron')->select(
                'SCHTYPE',
                'REGNO',
                'ABSDATE',
                'PERIODE',
                'BULAN',
                'DATEIN',
                'DATEOUT',
                'STATUS',
                'SCHIN',
                'SCHOUT'
            )->where('REGNO','=',$id[$counter]);
            $getResultKemarin = $getResultKemarin->where('ABSDATE', $newDateOver);
            $getResultKemarin = $getResultKemarin->get();
            if(sizeof($getResultKemarin)< 1 ){
                Rfid::query()
                ->where('nip', $id[$counter])
                ->whereDate('created_at',$newDateOver)->update([
                    'flag_read_s1' => '9',
                    
                ]);
                RfidTmp::query()
                ->where('nip', $id[$counter])
                ->whereDate('created_at',$newDateOver)->update([
                    'flag_read_s1' => '9']);
            }
            foreach($getResultKemarin as $key => $value) :
                $ResultSet[$value->REGNO][1] = $getResultKemarin;
            endforeach;
            $counter++;
        }
        return $ResultSet;
    }

    public function get_multiple_schedule($dateDetected, $arrayNIP, $arrayRecordId){
        $nowStart = Carbon::now()->format('H:i:s');
       print_r('[SPV] - START get_multiple_schedule() : '.$nowStart);
        $url = env('API_KESHC_URL', 'localhost').'api/inka/ht08-selected-date-multiple';
        $client = new Client();
        $formParamsToday = [
            'username' => env('API_KESHC_USERNAME', 'apikeshcinka_2018'),
            'password' => env('API_KESHC_PASSWORD', 'secretapikeshcinka2018'),
            'date' => $dateDetected,
            'nip' => $arrayNIP,
        ];
        try{
                $data = $client->request('POST', $url, array('form_params' => $formParamsToday));
                $resultToday =  json_decode($data->getBody()->getContents());
                $nowStop = Carbon::now()->format('Y-m-d H:i:s');
               print_r('[SPV] - STOP get_multiple_schedule() : '.$nowStop);
                return $resultToday;
        } catch (ServerException $e){
            //die();
            echo "WARNING: gagal memanggil API: ".$url."\n";
            // echo $e->getResponse()->getBody();
            foreach($arrayRecordId as $recordId) :
                RfidTmp::where('id', $recordId)->update(['flag_read_s1' => '3']);
            endforeach;
            $this->errorIndexTimeStamp  = 2;
        }
    }

    public function get_nip_fromTID( $tid){
        $user =User::where('rfid_tid',$tid)->first();
        $returnNIP= $user ? $user->nip : NULL;
        return $returnNIP;
    }

    public function getStatusAbsen($nip, $dateTimeDetected, $sch1, $sch2)
    {
        $range = false;
        $timeDetected = $dateTimeDetected->format('H:i:s');
        $dayDetected = $dateTimeDetected->format('j');
        $monthDetected = $dateTimeDetected->format('n');
        $yearDetected = $dateTimeDetected->format('Y');

        date_default_timezone_set("Asia/Bangkok");
        if(str_replace(' ', '', $sch1) == 'S1'){
            $shiftCode = $this->getHt01f($nip, $dayDetected, $monthDetected, $yearDetected);
            $rangeBySch = DB::table('rangebysch_ht04')
            ->where('schtype', $sch1)
            ->where('shiftcode', $shiftCode)
            ->get();
        }
        else{
            $rangeBySch = DB::table('rangebysch_ht04')->where('schtype', $sch1)->get();
        }

        $kemaren = [];
        $absen = [];
        $range_start = [];
        $range_end = [];
        for ($i = 0; $i < sizeof($rangeBySch); $i++) {
            $kemaren[$i] = $rangeBySch[$i]->kemaren;
            $absen[$i] = $rangeBySch[$i]->absen;
            $range_start[$i] = $rangeBySch[$i]->range_start;
            $range_end[$i] = $rangeBySch[$i]->range_end;
        }
        for ($i = 0; $i < sizeof($absen); $i++) {
            $date_start = Carbon::today()->format('Y-m-d') . " " . $range_start[$i];
            $date_start2 = new DateTime($date_start);
            $date_end = Carbon::today()->format('Y-m-d') . " " . $range_end[$i];
            $date_end2 = new DateTime($date_end);
            $time_detected = Carbon::today()->format('Y-m-d') . " " . $timeDetected;
            $time_detected2 = new DateTime($time_detected);
            if ($date_start2 <= $time_detected2 && $date_end2 >= $time_detected2) {
                // $range= new stdClass;
                $range["absen"] = $absen[$i];
                $range["kemaren"] = $kemaren[$i];
                $range["sch1"] = $sch1;
                $range["sch2"] = $sch2;
                $range["nip"] = $nip;

                break;
            }
        }

        return $range;
    }

    public function getHt01f($nip, $tanggal, $bulan, $tahun){
        $result = DB::table('ht01f_mirror')
        ->where('REGNO', $nip)
        ->where('PERIODE', $tahun)
        ->where('BULAN', $bulan)
        ->first();

        //convert dari stdObject ke array
        $result = json_decode(json_encode($result), true);
        $tanggal=$tanggal * 1;

        return $result['DAY'.$tanggal];
    }

    public function cek_exist_clean($tid, $schTodayDate, $status)
    {
        $return = '';
        try{
            $return = (RfidClean::where('rfid_tid', $tid)->where('status', $status)->where('tanggal', $schTodayDate)->exists() ? '1' : '0');
            // $return .= ' $tid : '.$tid.' $schTodayDate '.$schTodayDate.' $status '.$status;
        } catch(\Illuminate\Database\QueryException $ex) {
            $return = response()->json(['message' => $ex->getMessage()]);
        }
        return $return;
    }
    
    public function cek_exist_ht45_data_lembur($created_at, $nip){
        $getDateTimeDetected = new DateTime($created_at);
        $tahun = $getDateTimeDetected->format('Y');
        $bulan = $getDateTimeDetected->format('m');
        $tanggal = $getDateTimeDetected->format('j');
        
        $url = env('API_KESHC_URL', 'localhost').'api/inka/ht45-count-data';
        $client = new Client();
        $formParam = [
            'username' => env('API_KESHC_USERNAME'),
            'password' => env('API_KESHC_PASSWORD'),
            'regno' => $nip,
            'thn' => $tahun,
            'bln' => $bulan,
            'tgl' => $tanggal
        ];
        try{
            $data = $client->request('POST', $url, array('form_params' => $formParam));
            $resultCek =  json_decode($data->getBody()->getContents());
            return $resultCek;
        } catch (\Exception $e){
            // echo $e->getResponse()->getBody();
            echo $e->getMessage();
           print_r(json_encode($e));
        }
    }
    public function cekExistHt45KeshcLokal($created_at, $nip){
        $data=HT45KESHC::where('REGNO',$nip)
                ->where('TGLLOAD',$created_at)
                ->where('NOMESIN',999)
                ->get();
        return $data;
    }
    public function cek_exist_ht45_data($created_at, $nip){
        $url = env('API_KESHC_URL', 'localhost').'api/inka/ht45-exist-data';
        $client = new Client();
        $formParam = [
            'username' => env('API_KESHC_USERNAME'),
            'password' => env('API_KESHC_PASSWORD'),
            'regno' => $nip,
            'tglload' => $created_at.'.000'
        ];
        try{
            $data = $client->request('POST', $url, array('form_params' => $formParam));
            $resultCek =  json_decode($data->getBody()->getContents());
            $result = ($resultCek == []) ? 0 : $resultCek[0]->TGLLOAD;
            return $result;
        } catch (\Exception $e){
            echo $e->getResponse()->getBody();
        }
    }

    public function insert_ht45_device_rfid($id_tmp, $id_record, $nip, $kodeInOut, $dateTimeDetected){
        $configInsertHT45 = $this->configInsertHT45;
        
        if($configInsertHT45 == '1') :
            $url = env('API_KESHC_URL', 'localhost').'api/inka/ht45-insert';
            $client = new Client();
            $formParamsToday = [
                'username' => env('API_KESHC_USERNAME'),
                'password' => env('API_KESHC_PASSWORD'),
                'nip' => $nip,
                'kodeinout' => $kodeInOut,
                'tglload' => $dateTimeDetected
            ];
            $result = false;
            try{
                $data = $client->request('POST', $url, array('form_params' => $formParamsToday));
                $resultInsert =  json_decode($data->getBody()->getContents());
                RfidClean::where('id_record', $id_record)->update(['flag_ht45' => '1']);
               print_r('>>> sukses insert_ht45_device_rfid <<<');
               print_r(json_encode($resultInsert));
                // $result = true;
                return true;
            } catch (\Exception $e){
                //echo $e->getResponse()->getBody();
                //die();
                $this->errorIndexTimeStamp  = 2;
                echo "WARNING: gagal memasukkan tid: ".$id_record." || tglload : ".$dateTimeDetected." || now : ".date("Y-m-d H:i:s", strtotime("now"))." \n";
                echo $e->getMessage();
                $strDuplicate = "Cannot insert duplicate key row in object 'dbo.HT45' with unique index 'regnotglloadidx'.";
                if (strpos($e->getResponse()->getBody(), $strDuplicate) !== false) {
                    // duplicate handling
                    Rfid::where('id', $id_record)->update(['flag_read_s1' => '2']);
                    RfidTmp::where('id', $id_tmp)->update(['flag_read_s1' => '2']);
                    RfidClean::where('id_record', $id_record)->update(['flag_ht45' => '2']);
                } else {
                    RfidTmp::where('id', $id_tmp)->update(['flag_read_s1' => '0']);
                    echo "WARNING: gagal memasukkan tid: ".$id_tmp. "dengan pesan error berikut: ".$e->getResponse()->getBody();
                }
                // Rfid::where('id', $id_record)->update(['is_ht45' => '0']);
            }

            if(isset($resultInsert->response_code)) :
                RfidTmp::where('id', $id_record)->update(['flag_read_s1' => '0']);
                echo "WARNING: URL : ".$url."|| now : ".date("Y-m-d H:i:s", strtotime("now"))." \n";
                print_r($resultInsert);
            endif;
        else :
           print_r('[SPV] - >>> function insert_ht45_device_rfid DISABLED <<<');
           print_r('NIP : '.$nip.' || KODEINOUT : '.$kodeInOut);
        endif;
    }
    public function updateHt08Lokal($date, $nip, $kolom, $time, $recordId){
        $configUpdateHT08 = $this->configUpdateHT08;
        if($configUpdateHT08 == '1'){
            date_default_timezone_set("Asia/Bangkok");
            $date2 = explode(" ",$date);
            $timeKolom = '';
            $urlAPI = '';
            $statusAbsen = '';

            if($kolom == 'timein') : 
                $urlAPI = env('API_KESHC_URL', 'localhost').'api/update-timein';
                $timeKolom = 'time_in';
                $statusAbsen = 'masuk';
            endif;
            if($kolom == 'timeout') : 
                $urlAPI = env('API_KESHC_URL', 'localhost').'api/update-timeout';
                $timeKolom = 'time_out';
                $statusAbsen = 'pulang';
            endif;
        }
        $dataHt08=HT08MirroringCron::where('REGNO',$nip)
        ->whereDate('ABSDATE',$date2[0])->first();
        $dataHt08KeshcLokal=HT08KESHC::where('REGNO',$nip)
        ->whereDate('ABSDATE',$date2[0])->first();
        if($dataHt08KeshcLokal){
            if($kolom == 'timein') :
                HT08KESHC::where('REGNO',$nip)
                ->whereDate('ABSDATE',$date2[0])
                ->update([
                    'TIMEIN'=>$time,
                    
                ]);
            endif;
            if($kolom == 'timeout') :
                HT08KESHC::where('REGNO',$nip)
                ->whereDate('ABSDATE',$date2[0])
                ->update([
                    'TIMEOUT'=>$time,
                    
                ]);
            endif;
        }
        else{
            if($dataHt08){
                $createHt08Keshc= HT08KESHC::create([            
                    'WORKGRPCODE'=>$dataHt08->WORKGRPCODE,
                    'REGNO'=>$dataHt08->REGNO,
                    'ABSDATE'=>$dataHt08->ABSDATE,
                    'PERIODE'=>$dataHt08->PERIODE,
                    'BULAN'=>$dataHt08->BULAN,
                    'SUBNO'=>$dataHt08->SUBNO,
                    'ATTDSTAT'=>$dataHt08->ATTDSTAT,
                    'PERMITNO'=>$dataHt08->PERMITNO,
                    'OVERTIMENO'=>$dataHt08->OVERTIMENO,
                    'FURLOUGHCODE'=>$dataHt08->FURLOUGHCODE,
                    'FPERIODE'=>$dataHt08->FPERIODE,
                    'FURLOUGHNO'=>$dataHt08->FURLOUGHNO,
                    'DATEIN'=>$dataHt08->DATEIN,
                    'DATEOUT'=>$dataHt08->DATEOUT,
                    'TIMEIN'=>$dataHt08->TIMEIN,
                    'TIMEOUT'=>$dataHt08->TIMEOUT,
                    'STATUS'=>$dataHt08->STATUS,
                    'REMARK'=>$dataHt08->REMARK,
                    'SPECIALSCH'=>$dataHt08->SPECIALSCH,
                    'SPDNO'=>$dataHt08->SPDNO,
                    'SCHIN'=>$dataHt08->SCHIN,
                    'SCHOUT'=>$dataHt08->SCHOUT,
                    'OVERHOUR'=>$dataHt08->OVERHOUR,
                    'OVERCOUNT'=>$dataHt08->OVERCOUNT,
                    'PERMITST'=>$dataHt08->PERMITST,
                    'JAHIL'=>$dataHt08->JAHIL,
                    'JALEB'=>$dataHt08->JALEB,
                    'MEHIL'=>$dataHt08->MEHIL,
                    'ABCD'=>$dataHt08->ABCD,
                ]);
            }
            
            if($kolom == 'timein') :
                HT08KESHC::where('REGNO',$nip)
                ->whereDate('ABSDATE',$date2[0])
                ->update([
                    'TIMEIN'=>$time,
                    
                ]);
            endif;
            if($kolom == 'timeout') :
                HT08KESHC::where('REGNO',$nip)
                ->whereDate('ABSDATE',$date2[0])
                ->update([
                    'TIMEOUT'=>$time,
                    
                ]);
            endif;

        }
        $dataHt08KeshcLokal=HT08KESHC::where('REGNO',$nip)
        ->whereDate('ABSDATE',$date2[0])->first();
        return $dataHt08KeshcLokal;
    }
    public function update_ht08($date, $nip, $kolom, $time, $recordId){
        $configUpdateHT08 = $this->configUpdateHT08;
        if($configUpdateHT08 == '1') :
            date_default_timezone_set("Asia/Bangkok");
            $date2 = explode(" ",$date);
            $timeKolom = '';
            $urlAPI = '';
            $statusAbsen = '';

            if($kolom == 'timein') : 
                $urlAPI = env('API_KESHC_URL', 'localhost').'api/update-timein';
                $timeKolom = 'time_in';
                $statusAbsen = 'masuk';
            endif;
            if($kolom == 'timeout') : 
                $urlAPI = env('API_KESHC_URL', 'localhost').'api/update-timeout';
                $timeKolom = 'time_out';
                $statusAbsen = 'pulang';
            endif;
            
            $client = new Client();
            $formParamsToday = [
                'username' => env('API_KESHC_USERNAME', 'apikeshcinka_2018'),
                'password' => env('API_KESHC_PASSWORD', 'secretapikeshcinka2018'),
                'absdate' => $date2[0],
                'nip' => $nip,
                $timeKolom => $time,
            ];
            $client = new Client();
            $formParamsToday = [
                'username' => env('API_KESHC_USERNAME', 'apikeshcinka_2018'),
                'password' => env('API_KESHC_PASSWORD', 'secretapikeshcinka2018'),
                'absdate' => $date2[0],
                'nip' => $nip,
                $timeKolom => $time,
            ];

	        try{
            	$data = $client->request('POST', $urlAPI, array('form_params' => $formParamsToday));
                $result_update =  json_decode($data->getBody()->getContents());
                return $result_update;
	        } catch (\Exception $e){
                //die();
                echo "WARNING: gagal memanggil API: ".$urlAPI." : ".date("Y-m-d H:i:s", strtotime("now"))." \n";
                echo $e->getResponse()->getBody();
                $this->errorIndexTimeStamp  = 2;
                RfidTmp::where('id', $recordId)->update(['flag_read_s1' => '0']);
            }
            //print_r("update HT08 || NIP : ".$nip." || DATE : ".$date." || KOLOM : ".$kolom." || TIME : ".$time);
        else : 
            return true;
           print_r('[SPV] - >>> function update_ht08 DISABLED <<<');
        endif;

	        
    }
    public function insertHt45Lokal($id_tmp, $id_record, $nip, $kodeInOut, $dateTimeDetected){
        $configInsertHT45 = $this->configInsertHT45;
        $create = NULL;
        if($configInsertHT45 == '1'){
            $tglLoad= new Carbon($dateTimeDetected);
            $formInsert = [
               
                'NOLOG' =>($tglLoad->format('Y')%1000).$tglLoad->format('m').'0'.rand(10000,99999),
                'REGNO' => $nip,
                'TIPE' => (string)rand(0, 3),
                'TGLLOAD'=>$dateTimeDetected,
                'NOMESIN'=>999,
                'IDFINGER'=>$nip,
                'NAMANYA'=>NULL,
                'INDEKSFINGER'=>0,
                'MODEVERIFIKASI'=>1,
                'OTORITAS'=>0,
                'KODEINOUT'=>$kodeInOut,
                'THN'=>$tglLoad->format('Y'),
                'BLN'=>$tglLoad->format('m'),
                'TGL'=>$tglLoad->format('d'),
                'JAM'=>$tglLoad->format('h'),
                'MENIT'=>$tglLoad->format('i'),
                'DETIK'=>$tglLoad->format('s'),
                'LOGVERIF'=>'1',
            ];
            $create=HT45KESHC::create($formInsert);

        }
        return $create;
    }

    public function update_clean_pulang($tid, $status, $tanggal, $created_at, $id_record, $reader_mac)
    {
        $configUpdateCleanPulang = '1';
        if($configUpdateCleanPulang == '1') :
            RfidClean::where('rfid_tid', $tid)->where('status', $status)->where('tanggal', $tanggal)->update([
                'created_at' => $created_at,
                'created_at_pulang' => $created_at,
                'id_record' => $id_record,
                'rfid_reader_mac' => $reader_mac
            ]);
        else :
           print_r('[SPV] - >>> function update_clean_pulang DISABLED <<<');
        endif;
    }

    public function proses_lembur($id_tmp, $id_kotor, $nip, $created_at, $tid, $tanggal)
    {
        $updateRFIDTmp = RfidTmp::where('id', $id_tmp)->update([
            'nip' => $nip,
            'flag_read_s1' => '1',
            'is_libur'  => '1',
            'is_ht45'  => '0'
        ]);
        $updateRFIDKotor = Rfid::where('id', $id_kotor)->update([
            'nip' => $nip,
            'flag_read_s1' => '1',
            'is_libur'  => '1',
            'is_ht45'  => '0'
        ]);
       print_r('function proses_lembur(id_tmp = '.$id_tmp.', id_kotor = '.$id_kotor.', nip = '.$nip.', created_at = '.$created_at.', tid = '.$tid.', tanggal = '.$tanggal.')');
        
        $existDataHt45Lembur = $this->cek_exist_ht45_data_lembur($created_at, $nip);
       print_r('$existDataHt45Lembur = '.$existDataHt45Lembur);
        if($existDataHt45Lembur > 1) :
            $insert_ht45 = $this->insertHt45Lokal($id_tmp, $id_kotor, $nip, '1', $created_at);
            //inseet ht45 lokal
            if ($insert_ht45) :
               print_r('$insert_ht45 : update is_ht45');
                RfidTmp::where('id', $id_tmp)->update(['is_ht45' => '1']);
                Rfid::where('id', $id_kotor)->update(['is_ht45' => '1']);
            endif;
        else :
            $getDateTimeDetected = new DateTime($created_at);
            $tahun = $getDateTimeDetected->format('Y');
            $bulan = $getDateTimeDetected->format('m');
            $tanggal = $getDateTimeDetected->format('j');
            $url = env('API_KESHC_URL', 'localhost').'api/inka/ht45-select-exist-data';
            $client = new Client();
            $paramsSelectExist = [
                'username' => env('API_KESHC_USERNAME'),
                'password' => env('API_KESHC_PASSWORD'),
                'regno' => $nip,
                'thn' => $tahun,
                'bln' => $bulan,
                'tgl' => $tanggal
            ];
            try{
                $data = $client->request('POST', $url, array('form_params' => $paramsSelectExist));
                $resultSelect =  json_decode($data->getBody()->getContents());
                
                $url = env('API_KESHC_URL', 'localhost').'api/inka/ht45-update-data';
                $client = new Client();
                $paramsLembur = [
                    'regno' => $nip,
                    'username' => env('API_KESHC_USERNAME'),
                    'password' => env('API_KESHC_PASSWORD'),
                    'tglload_lama' => $resultSelect[0]->TGLLOAD,
                    'tglload' => $created_at,
                ];
                try{
                    $data = $client->request('POST', $url, array('form_params' => $paramsLembur));
                    $resultInsert =  json_decode($data->getBody()->getContents());

                    if($resultInsert) :
                       print_r('$resultInsert : update is_ht45');
                        RfidTmp::where('id', $id_tmp)->update(['is_ht45'  => '1']);
                        Rfid::where('id', $id_kotor)->update(['is_ht45'  => '1']);
                    endif;
                } catch  (\Exception $e){
                    echo "WARNING: gagal update data ht45 lembur: id_tmp = ".$id_tmp." || tglload : ".$created_at." || now : ".date("Y-m-d H:i:s", strtotime("now"))." \n";
                    echo $e->getMessage();
                }

            } catch (\Exception $e){
                echo "WARNING: gagal select ht45-select-exist-data";
                echo $e->getMessage();
            }
        endif;
    }
    public function prosesLiburShift($nip,$tanggal){
        $tanggalToday=new Carbon($tanggal);
        $curProcessDay=new Carbon($tanggal);
        $tanggalYesterday=(new DateTime($curProcessDay->format('Y-m-d')))->modify('-1 day');

        $schtypeKemarin=HT08MirroringCron::where([
            ['REGNO',$nip],
            ['ABSDATE',$tanggalYesterday->format('Y-m-d')]])->first();
        $schtype=HT08MirroringCron::where([
            ['REGNO',$nip],
            ['ABSDATE',$tanggalToday->format('Y-m-d')]])->first();

            if($schtype){
                if(str_contains($schtype->STATUS, 'L')){
                    $tanggalDetected=$kemarin->format('d');
                    $bulanDetected=$kemarin->format('m');
                    $tahunDetected=$kemarin->format('Y');
                    $shiftCode = $supervisor->getHt01f($nip, $tanggalDetected, $bulanDetected, $tahunDetected);
                    if($schtypeKemarin){
                        $absen = DB::table('rangebysch_ht04')
                        ->where('schtype', $schtypeKemarin->SCHTYPE)
                        ->where('shiftcode', $shiftCode)
                        ->where('absen','2')
                        ->where('kemaren','1')
                        ->first();
                        print_r($absen);
                        $output = print_r($absen,true);
                        $createLog=LogProsesLiburShift::create([
                            'nip'=>$nip,
                            'tanggal_proses'=>$curProcessDay->format('Y-m-d'),
                        ]);
                        if($absen){
                            echo "data absen ditemukan";
                            $formParamsToday = [
                                'username' => env('API_KESHC_USERNAME', 'apikeshcinka_2018'),
                                'password' => env('API_KESHC_PASSWORD', 'secretapikeshcinka2018'),
                                'nip' => $data->nip,
                                'shift_kemarin'=>1,
                                'range_start_pulang'=>$curProcessDay->format('Y-m-d').' '.$absen->range_start,
                                'range_end_pulang'=>$curProcessDay->format('Y-m-d').' '.$absen->range_end,
                                'tanggal'=>$curProcessDay->format('Y-m-d'),
                            ];
                            
                        }
                        else{
                            echo "data absen tidak ditemukan"; 
                            $formParamsToday = [
                                'username' => env('API_KESHC_USERNAME', 'apikeshcinka_2018'),
                                'password' => env('API_KESHC_PASSWORD', 'secretapikeshcinka2018'),
                                'nip' => $data->nip,
                                'shift_kemarin'=>0,
                                'range_start_pulang'=>NULL,
                                'range_end_pulang'=>NULL,
                                'tanggal'=>$curProcessDay->format('Y-m-d'),
                                
                            ];

                        }
                        print_r($formParamsToday);
                        $output = print_r($formParamsToday,true);
                        LogProsesLiburShift::where('id',$createLog->id)->update([
                            'form_params'=>$output,
                        ]);
                        // if(str_contains(public_path(),'localhost')){
                            // print_r('input kesh lokal');
                            // $this->inputHt45KeshcLokal($formParamsToday);

                        // }


                        try{
                            $apikeshc = $client->request('POST', $url, array('form_params' => $formParamsToday));
                           print_r(">>> sukses ubah kodeinout ht45 ".$data->nip ." <<<");
                        } catch (\Exception $e){
                            echo "WARNING: gagal ubah kodeinout ht45 ".$data->nip."\n";
                                // echo $e->getMessage();
                                print_r($e->getResponse());
                                $output = print_r($e->getResponse(),true);
                                LogProsesLiburShift::where('id',$createLog->id)->update([
                                    'hasil'=>$output,
                                ]);
                        }
                    }
                    
                }
                else{
                    $createLog=LogProsesLiburShift::create([
                        'nip'=>$nip,
                        'tanggal_proses'=>$curProcessDay->format('Y-m-d'),
                        'hasil'=>'Bukan Hari Libur',
                    ]);
                }
            }
    }
    public function sincFlagReadTmpAndReal($idKotor=NULL,$idTmp=NULL){

       $rfidTmp     =RfidTmp::where('id', $idTmp)->first();
       $rfidRiwayat =Rfid::where('id', $idKotor)->first();

       if($rfidTmp && $rfidRiwayat){
           if($rfidTmp->flag_read_s1!=$rfidRiwayat->flag_read_s1){
                RfidTmp::where('id', $idTmp)->update(['flag_read_s1'=>'0']); 
                Rfid::where('id', $idKotor)->update(['flag_read_s1'=>'0']);
           }
       }
    }
    public function sincAllFlagReadTmpAndReal($date){
        $dataKotor=Rfid::select('nip','rfid_tid','created_at')->whereNull('flag_read_s1')->whereDate('created_at',$date)->get();
        foreach($dataKotor as $value){
            $updateTmp=RfidTmp::where('rfid_tid', $value->rfid_tid)
            ->where('created_at',$value->created_at->format('Y-m-d H:i:s'))
            ->update(['flag_read_s1'=>'0']);
            $value->flag_read_s1='0';
            $value->save();
            $this->absensi_fraud->insertToFraudTable($nip,$fraudOn,$tanggal,$schtype);
        }

    }

    public function rfid($nip,$time,$message)
    {
        $configNotif = '0';
        // $configNotif = $this->configNotif;
        if($nip == '991700051') :
            if($configNotif == '1') :
                $user = User::where('nip', $nip)->first();
                $rfidClean = RfidClean::where('rfid_tid', $user->rfid_tid)->orderBy('created_date', 'asc')->first();
                if ($user) :
                    $url=url('/');
                    $server='';
                    // if(str_contains($url,'apis')){
                    // $server=' (apis)';
                    // }
                    $title = 'INKA Mobile'.$server;
                    $body = 'Anda absen '.$message.' pada '.$time.$server;
                    $label = 'rfid detected';
                    $user->notify(new FirebasePushNotif($title, $body, $label));
                endif;
            else :
               print_r('[SPV] - >>> function Notification RFID DISABLED <<<');
            endif;
        endif;
    }
    public function isInsertClean($paramDB=[]){
        $dataClean=RfidClean::where('id_record',$paramDB['id_record'])->first();
        if(!$dataClean){
            $insertRfidClean = RfidClean::insert($paramDB);
        }

    }
    public function isInsertCleanLive($paramDB=[]){
        $dataClean=RfidClean::where('id_record',$paramDB['id_record'])->first();
        if(!$dataClean){
            $insertRfidClean = RfidClean::insert($paramDB);
        }

    }
    public function prosesLiburShiftLokal($nip,$idKotor,$idTmp,$timeStamp){
        $tanggalToday=new Carbon($timeStamp);
        $curProcessDay=new Carbon($timeStamp);
        $tanggalYesterday=(new DateTime($curProcessDay->format('Y-m-d')))->modify('-1 day');
        $kemarin=$tanggalYesterday;

        $schtypeKemarin=HT08MirroringCron::where([
            ['REGNO',$nip],
            ['ABSDATE',$tanggalYesterday->format('Y-m-d')]])->first();
        $schtype=HT08MirroringCron::where([
            ['REGNO',$nip],
            ['ABSDATE',$tanggalToday->format('Y-m-d')]])->first();

            if($schtype){
                if(str_contains($schtype->STATUS, 'L')){
                    $tanggalDetected=$kemarin->format('d');
                    $bulanDetected=$kemarin->format('m');
                    $tahunDetected=$kemarin->format('Y');
                    $shiftCode = $this->getHt01f($nip, $tanggalDetected, $bulanDetected, $tahunDetected);
                    if($schtypeKemarin){
                        $absen = DB::table('rangebysch_ht04')
                        ->where('schtype', $schtypeKemarin->SCHTYPE)
                        ->where('shiftcode', $shiftCode)
                        ->where('absen','2')
                        ->where('kemaren','1')
                        ->first();
                        print_r($absen);
                        $output = print_r($absen,true);
                        $createLog=LogProsesLiburShift::create([
                            'nip'=>$nip,
                            'tanggal_proses'=>$curProcessDay->format('Y-m-d'),
                        ]);
                        if($absen){
                            $waktuTimeStamp= new DateTime($timeStamp);
                            $waktuRangeEnd=new DateTime($curProcessDay->format('Y-m-d').' '.$absen->range_end);
                            $waktuRangeStart= new DateTime($curProcessDay->format('Y-m-d').' '.$absen->range_start);
                            

                            if($waktuTimeStamp >= $waktuRangeStart && $waktuTimeStamp <= $waktuRangeEnd){
                                
                                $this->insertHt45Lokal($idTmp, $idKotor, $nip, 1, $timeStamp);
                                $this->updateHt08Lokal($tanggalYesterday->format('Y-m-d'),$nip, 'timeout', $timeStamp, $idKotor);
                                $dataRfid=Rfid::where('id',$idKotor)->first();
                                if($dataRfid){
                                    $paramDB['id_record'] = $idKotor;
                                    $paramDB['rfid_epc']=$dataRfid->rfid_epc;
                                    $paramDB['rfid_tid']=$dataRfid->rfid_tid;
                                    $paramDB['rfid_userdata']=$dataRfid->rfid_userdata;
                                   
                                    $paramDB['rfid_reader_ip']=$dataRfid->rfid_reader_ip;
                                    $paramDB['rfid_reader_mac']=$dataRfid->rfid_reader_mac;
                                    $paramDB['rfid_reader_ant']=$dataRfid->rfid_reader_ant;
                                    $paramDB['nip']=$nip;
                                    $paramDB['tanggal']=$tanggalYesterday->format('Y-m-d');
                                    $paramDB['status'] = 'pulang';
                                    $paramDB['created_at_pulang'] = $timeStamp;
                                    $paramDB['flag_ht08'] = '1';
                                    RfidClean::insert($paramDB);

                                }
                                
                            }
                            elseif($waktuTimeStamp > $waktuRangeEnd){
                                $dataHT45Keshc=HT45KESHC::where('REGNO',$nip)
                                ->where('KODEINOUT',0)
                                ->whereDate('TGLLOAD',$curProcessDay->format('Y-m-d'))
                                ->first(); 
                                if($dataHT45Keshc){
                                    $this->insertHt45Lokal($idTmp, $idKotor, $nip, 1, $timeStamp);
                                }
                                else{
                                    $this->insertHt45Lokal($idTmp, $idKotor, $nip, 0, $timeStamp);

                                }
                            }
                               
                        }
                        else{
                           
                            $dataHT45Keshc=HT45KESHC::where('REGNO',$nip)
                            ->where('KODEINOUT',0)
                            ->whereDate('TGLLOAD',$curProcessDay->format('Y-m-d'))
                            ->first(); 
                            if($dataHT45Keshc){
                                $this->insertHt45Lokal($idTmp, $idKotor, $nip, 1, $timeStamp);
                            }
                            else{
                                $this->insertHt45Lokal($idTmp, $idKotor, $nip, 0, $timeStamp);

                            }

                        }
                        Rfid::where('id', $idKotor)->update([
                            
                            'flag_read_s1' => '1',
                           
                        ]);
                        RfidTmp::where('id', $idTmp)->update([
                            'flag_read_s1' => '1',
                            
                           
                        ]); 
                    }
                    
                }
                else{
                    $createLog=LogProsesLiburShift::create([
                        'nip'=>$nip,
                        'tanggal_proses'=>$curProcessDay->format('Y-m-d'),
                        'hasil'=>'Bukan Hari Libur',
                    ]);
                }
            }
        
    }
    public function insertToFraudRiwayatNull($arrIdKotor=[]){
        $data=DB::table('ht45_rfid')->select('created_at','nip','rfid_tid')->whereIn('id', $arrIdKotor)
            ->whereNull('flag_read_s1')
            ->get();
        
        $idFraud=[];
        $dateNow=Carbon::now();
        foreach ($data as $key => $value) {
            if(is_null($value->nip)){
                $user = User::where('rfid_tid',$value->rfid_tid)->first();
                if(is_null($user)){
                    continue;
                }
                else{
                    $value->nip=$user->nip;
                }
            }
            $dateTimeCur=new DateTime( $value->created_at);
            $arrDateTime['cur_time']=new DateTime( $value->created_at);
            $dateInterval=new DateInterval('P1D');
            
            $yesterDayDateTime= $dateTimeCur->sub($dateInterval);
            
            $arrDateTime['ytd_time']=$yesterDayDateTime;
            
            
            $dataJadwal = HT08MirroringCron::where('REGNO',$value->nip)
            ->where(function($query) use ($arrDateTime){
                $query->whereDate('ABSDATE' ,'=', $arrDateTime['cur_time']->format('Y-m-d'));
                $query->orWhereDate('ABSDATE' ,'=',$arrDateTime['ytd_time']->format('Y-m-d'));
            })->orderBy('ABSDATE','DESC')->get();
            
            foreach ($dataJadwal as $keyJadwal => $valueJadwal) {
                $createMasuk=AbsensiFraud::create([
                    'nip'=>$value->nip,
                    'tanggal_absen'=>$valueJadwal->ABSDATE,
                    'tipe_absen'=>$valueJadwal->SCHTYPE,
                    'fraud_on'=>0,
                ]);
                array_push($idFraud,$createMasuk->id);
                $createPulang=AbsensiFraud::create([
                    'nip'=>$value->nip,
                    'tanggal_absen'=>$valueJadwal->ABSDATE,
                    'tipe_absen'=>$valueJadwal->SCHTYPE,
                    'fraud_on'=>1,
                ]);
                array_push($idFraud,$createPulang->id);
            }

        }
        return $idFraud;

    }
    public function handleFraudRfidReaderMac($date=NULL){
        
        
        
        
        $masterReader=MasterRfidReader::all();
        foreach ($masterReader as $key => $value) {
            $updateKotor=Rfid::where('rfid_reader_ip','LIKE',$value->rfid_reader_ip.'%');
            if($date){
                $updateKotor=$updateKotor->whereDate('created_at',$date);
            }
            $updateKotor=$updateKotor->update([
                'rfid_reader_mac' => $value->rfid_reader_mac,
            ]);
            $updateTmp=RfidTmp::where('rfid_reader_ip','LIKE',$value->rfid_reader_ip.'%');
            if($date){
                $updateTmp=$updateTmp->whereDate('created_at',$date);
            }
            $updateTmp=$updateTmp->update([
                'rfid_reader_mac' => $value->rfid_reader_mac,
            ]);
        }
    }
    public function insertHt08StatusNonaktif($dateStart,$dateEnd){
        $ht08Mirroring=HT08MirroringCron::whereIn('STATUS',$this->status_libur)
        ->where([
            ['ABSDATE','<=',$dateEnd],
            ['ABSDATE','>=',$dateStart],
        ])->get();

        foreach ($ht08Mirroring as $key => $dataHt08) {
            $createHt08Keshc= HT08KESHC::updateOrCreate([
                
                    'REGNO'=>$dataHt08->REGNO,
                    'ABSDATE'=>$dataHt08->ABSDATE,
            ],            
            [

                'WORKGRPCODE'=>$dataHt08->WORKGRPCODE,
                
                'PERIODE'=>$dataHt08->PERIODE,
                'BULAN'=>$dataHt08->BULAN,
                'SUBNO'=>$dataHt08->SUBNO,
                'ATTDSTAT'=>$dataHt08->ATTDSTAT,
                'PERMITNO'=>$dataHt08->PERMITNO,
                'OVERTIMENO'=>$dataHt08->OVERTIMENO,
                'FURLOUGHCODE'=>$dataHt08->FURLOUGHCODE,
                'FPERIODE'=>$dataHt08->FPERIODE,
                'FURLOUGHNO'=>$dataHt08->FURLOUGHNO,
                'DATEIN'=>$dataHt08->DATEIN,
                'DATEOUT'=>$dataHt08->DATEOUT,
                'TIMEIN'=>$dataHt08->TIMEIN,
                'TIMEOUT'=>$dataHt08->TIMEOUT,
                'STATUS'=>$dataHt08->STATUS,
                'REMARK'=>$dataHt08->REMARK,
                'SPECIALSCH'=>$dataHt08->SPECIALSCH,
                'SPDNO'=>$dataHt08->SPDNO,
                'SCHIN'=>$dataHt08->SCHIN,
                'SCHOUT'=>$dataHt08->SCHOUT,
                'OVERHOUR'=>$dataHt08->OVERHOUR,
                'OVERCOUNT'=>$dataHt08->OVERCOUNT,
                'PERMITST'=>$dataHt08->PERMITST,
                'JAHIL'=>$dataHt08->JAHIL,
                'JALEB'=>$dataHt08->JALEB,
                'MEHIL'=>$dataHt08->MEHIL,
                'ABCD'=>$dataHt08->ABCD,
            ]);
        }
        
        
        
    }

    public function triggerFingerHt45ToHt08($arrParse=[]){
        $dateInterval= new DateInterval('P1D');
        $dateIntervalHourStart= new DateInterval('PT9H');
        $dateIntervalHourEnd= new DateInterval('PT10H');
        if(isset($arrParse['id'])){
            $dataHt45=HT45KESHC::where('id',$arrParse['id'])->first();
            if($dataHt45){
                if($dataHt45->KODEINOUT==0 || $dataHt45->KODEINOUT=='0'){
                    $dateTimeTglLoad= new DateTime($dataHt45->TGLLOAD);
                    HT08KESHC::where('REGNO',$dataHt45->REGNO)
                    ->whereDate('ABSDATE',$dateTimeTglLoad->format('Y-m-d'))
                    ->whereNull('TIMEIN')
                    ->update(['TIMEIN'=>$dateTimeTglLoad->format('Y-m-d H:i:s')]);
                    $dataHt08=HT08KESHC::where('REGNO',$dataHt45->REGNO)
                    ->whereDate('ABSDATE',$dateTimeTglLoad->format('Y-m-d'))->first();
                    

                    $arrParse['tanggal_absen']=$dateTimeTglLoad->format('Y-m-d');
                    $arrParse['data_ht08']=$dataHt08;
                    $arrParseToPrd['REGNO']            = $dataHt45->REGNO;
                    $arrParseToPrd['ABSDATE']          = $dateTimeTglLoad->format('Y-m-d');
                    $arrParseToPrd['datetimedetected'] = $dateTimeTglLoad->format('Y-m-d H:i:s');
                    $arrParseToPrd['kodeinout']        = $dataHt45->KODEINOUT;
                    $arrParseToPrd['nomesin']          = 990;
                    $arrParseToPrd['kolom']            ='timein';
                    $inputHt08Prd = $this->injectHt08Prd($arrParseToPrd);
                    //$inputHt45Prd = $this->injectHt45Prd($arrParseToPrd);

                }
                if($dataHt45->KODEINOUT==1 || $dataHt45->KODEINOUT=='1' ){
                    $dateTimePrev=new DateTime($dataHt45->TGLLOAD);
                    $dateTimePrev=$dateTimePrev->sub($dateInterval);
                    $dateTimeCur=new DateTime($dataHt45->TGLLOAD);
                    $dateTimeNext=new DateTime($dataHt45->TGLLOAD);
                    $dateTimeNext=$dateTimeNext->add($dateInterval);
                    $dateRange=new stdClass;
                    $dateRange->start=$dateTimePrev->format('Y-m-d');
                    $dateRange->end=$dateTimeNext->format('Y-m-d');
                    $dataHt08=HT08KESHC::where('REGNO',$dataHt45->REGNO)
                    ->where(function($query) use ($dateRange){
                        $query->whereDate('ABSDATE','<=',$dateRange->end);
                        $query->whereDate('ABSDATE','>=',$dateRange->start);
                    })->get();
                    foreach($dataHt08 as $jadwal){
                        if(is_null($jadwal->SCHOUT)){
                            continue;
                        }
                        $dateTimeSchoutStart= new DateTime($jadwal->SCHOUT);
                        $dateTimeSchoutStart= $dateTimeSchoutStart->sub($dateIntervalHourStart);
                        $dateTimeSchoutEnd= new DateTime($jadwal->SCHOUT);
                        $dateTimeSchoutEnd= $dateTimeSchoutEnd->add($dateIntervalHourEnd);
                        if($dateTimeCur < $dateTimeSchoutEnd && $dateTimeCur >= $dateTimeSchoutStart){
                            $dateTimeChange=$dateTimeCur;

                            if($jadwal->TIMEOUT){
                                $dateTimeCurTimeOut= new DateTime($jadwal->TIMEOUT);
                                if($dateTimeCur < $dateTimeCurTimeOut){
                                    $dateTimeChange=$dateTimeCurTimeOut;
                                }
                            }
                            $jadwal->TIMEOUT=$dateTimeChange->format('Y-m-d H:i:s');
                            $jadwal->save();
                            $dateTimeAbsdateToPrd= new DateTime($jadwal->ABSDATE);
                            $arrParseToPrd['REGNO']            = $dataHt45->REGNO;
                            $arrParseToPrd['ABSDATE']          = $dateTimeAbsdateToPrd->format('Y-m-d');
                            $arrParseToPrd['datetimedetected'] = $dateTimeChange->format('Y-m-d H:i:s');
                            $arrParseToPrd['kodeinout']        = $dataHt45->KODEINOUT;
                            $arrParseToPrd['nomesin']          = 991;
                            $arrParseToPrd['kolom']            ='timeout';
                            $inputHt08Prd = $this->injectHt08Prd($arrParseToPrd);
                            //$inputHt45Prd = $this->injectHt45Prd($arrParseToPrd);
                            $arrParse['data_ht08']=$jadwal;
                            $arrParse['tanggal_absen']=$jadwal->ABSDATE;
                            break;
                        }
                    }
                   
                    


                }
            }
        }
        return $arrParse;
    }
    public function insertHt45LokalCustom($arrParse=[]){
        $id_tmp=$arrParse['id_tmp'];
        $id_record=$arrParse['id_record'];
        $nip=$arrParse['nip'];
        $kodeInOut=$arrParse['kodeinout'];
        $dateTimeDetected=$arrParse['datetimedetected'];
        $noMesin=$arrParse['nomesin'];
        $configInsertHT45 = $this->configInsertHT45;
        $create = NULL;
        if($configInsertHT45 == '1'){
            $tglLoad= new Carbon($dateTimeDetected);
            $formInsert = [
               
                'NOLOG' =>($tglLoad->format('Y')%1000).$tglLoad->format('m').'0'.rand(10000,99999),
                'REGNO' => $nip,
                'TIPE' => (string)rand(0, 3),
                'TGLLOAD'=>$dateTimeDetected,
                'NOMESIN'=>$noMesin,
                'IDFINGER'=>$nip,
                'NAMANYA'=>NULL,
                'INDEKSFINGER'=>0,
                'MODEVERIFIKASI'=>1,
                'OTORITAS'=>0,
                'KODEINOUT'=>$kodeInOut,
                'THN'=>$tglLoad->format('Y'),
                'BLN'=>$tglLoad->format('m'),
                'TGL'=>$tglLoad->format('d'),
                'JAM'=>$tglLoad->format('h'),
                'MENIT'=>$tglLoad->format('i'),
                'DETIK'=>$tglLoad->format('s'),
                'LOGVERIF'=>'1',
            ];
            $create=HT45KESHC::create($formInsert);

        }
        return $create;
    }

    public function injectHt08Prd($arrParse=[]){
        $formParams=[
            
            'REGNO'     => $arrParse['REGNO'],
            'ABSDATE'   => $arrParse['ABSDATE'],
        ];
        if($arrParse["kolom"]=='timein'){
            $formParams['TIMEIN']=$arrParse['datetimedetected'];
        }
        if($arrParse["kolom"]=='timeout'){
            $formParams['TIMEOUT']=$arrParse['datetimedetected'];
        }

        $url=$this->url.'inject-ht08-local';
        
        try{
            $getDataJson = $this->client->request('POST', $url, array('form_params' => $formParams));
        } catch (\Exception $e){
            echo "WARNING: gagal update kolom ".$arrParse["kolom"]."ht08 prd \n";
            print_r($arrParse);
        }
        $getData=NULL;
        if(isset($getDataJson)){
            $getData =  json_decode($getDataJson->getBody()->getContents(), true);
            
        }
        return $getData;
    }
    public function injectHt45Prd($arrParse=[]){
        $formParams=[
            
            'nip'         => $arrParse['REGNO'],
            'kodeinout'   => $arrParse['kodeinout'],
            'nomesin'     => $arrParse['nomesin'],
            'datetimedetected'=>$arrParse['datetimedetected'],
        ];
        $url=$this->url.'inject-ht45-local';
        
        try{
            $getDataJson = $this->client->request('POST', $url, array('form_params' => $formParams));
        } catch (\Exception $e){
            echo "WARNING: gagal insert ht45 prd "." \n";
            print_r($arrParse);
        }
        $getData=NULL;
        if(isset($getDataJson)){
            $getData =  json_decode($getDataJson->getBody()->getContents(), true);
            
        }
        return $getData;
    }
    public function insertClean($arrCreate=[],$status,$createdAtMasuk,$createdAtPulang,$tanggal){
        $isAda=RfidClean::whereDate('tanggal',$tanggal)
        ->where([
            ['rfid_tid',$arrCreate['rfid_tid']],
            ['status',$status]
        ])->first();
        if($isAda){
            $arrUpdate=[
                'id_record'=>$arrCreate['id'],
                // 'rfid_epc'=>$arrCreate['rfid_epc'],
                // 'rfid_userdata'=>$arrCreate['rfid_userdata'],
                'rfid_reader_ip'=>$arrCreate['rfid_reader_ip'],
                'rfid_reader_mac'=>$arrCreate['rfid_reader_mac'],
                // 'rfid_reader_ant'=>$arrCreate['rfid_reader_ant'],
                'created_at_masuk'=>$createdAtMasuk,
                'created_at_pulang'=>$createdAtPulang,
                'created_at'=>$createdAtMasuk ? $createdAtMasuk : $createdAtPulang,
                'flag_ht08' => '1',
                'flag_ht45'=>NULL,
            ];
            try{
                RfidClean::whereDate('tanggal',$tanggal)
                ->where([
                    ['rfid_tid',$arrCreate['rfid_tid']],
                    ['status',$status]
                ])->update($arrUpdate);
            } catch(\Exception $e){
                sleep(5);
                return 0;
            }
            
            return $isAda->id;
        }
        try{
            $clean=RfidClean::create([
                'id_record'=>$arrCreate['id'],
                'nip'=>$arrCreate['nip'],
                // 'rfid_epc'=>$arrCreate['rfid_epc'],
                'rfid_tid'=>$arrCreate['rfid_tid'],
                // 'rfid_userdata'=>$arrCreate['rfid_userdata'],
                'rfid_reader_ip'=>$arrCreate['rfid_reader_ip'],
                'rfid_reader_mac'=>$arrCreate['rfid_reader_mac'],
                // 'rfid_reader_ant'=>$arrCreate['rfid_reader_ant'],
                'status'=>$status,
                'created_at_masuk'=>$createdAtMasuk,
                'created_at_pulang'=>$createdAtPulang,
                'tanggal'=>$tanggal,
                'created_at'=>$createdAtMasuk ? $createdAtMasuk : $createdAtPulang,
                'flag_ht08' => '1',
                'flag_ht45'=>NULL,
            ]);
        } catch(\Exception $e){
          sleep(5);
          return 0;
        }
        
        
        
        
        return $clean->id;
    }
        //dd($clean->id);
    public function insertHt45RfidIgnoreError($id_tmp, $id_record, $nip, $kodeInOut, $dateTimeDetected){
            $url = env('API_KESHC_URL', 'localhost').'api/inka/ht45-insert';
            $client = new Client();
            $formParamsToday = [
                'username' => env('API_KESHC_USERNAME'),
                'password' => env('API_KESHC_PASSWORD'),
                'nip' => $nip,
                'kodeinout' => $kodeInOut,
                'tglload' => $dateTimeDetected
            ];
            $result = false;
            try{
                $data = $client->request('POST', $url, array('form_params' => $formParamsToday));
                $resultInsert =  json_decode($data->getBody()->getContents());
                RfidClean::where('id_record', $id_record)->update(['flag_ht45' => '1']);
                
                // $result = true;
                return true;
            } catch (\Exception $e){
                
                
                $strDuplicate = "Cannot insert duplicate key row in object 'dbo.HT45' with unique index 'regnotglloadidx'.";
                if($e->getResponse()){
                    if (strpos($e->getResponse()->getBody(), $strDuplicate) !== false) {
                        // duplicate handling
                        Rfid::where('id', $id_record)->update(['flag_read_s1' => '2']);
                       
                        RfidClean::where('id_record', $id_record)->update(['flag_ht45' => '2']);
                    } else {
                        
                        
                    }

                }
                
                
            }

            
    }
    public function createHt45RfidXExtractJadwal($arrParse){
        $data=Ht45RfidXExtractJadwal::create($arrParse);
        $return= new stdClass;
        $return->response_code=200;
        $return->data=$data;
    }
    public function createTresholdPerformanceSpvUpload($arrParse){
        $data=TresholdPerformanceSpvUpload::create($arrParse);
        $return= new stdClass;
        $return->response_code=200;
        $return->data=$data;
    }
    public function tresholdingHt45RfidXExtractJadwal(Ht45RfidXExtractJadwal $ht45XExtractJadwal){
        /*[
            'id'
            'id_extract_jadwal'
            'id_ht45_rfid_tmp'
            'tanggal_absen'
            'nip'
            'status_pulang_masuk'
            'date_time_detected'
            'created_at_rfid_tmp'
            'rfid_tid'
            'rfid_reader_mac'
            'rfid_reader_ip'
            'flag_notif'
            'flag_upload'
        ]*/
        $return = new stdClass;
        $return->data=NULL;
        $dateTimeRfid= new DateTime($ht45XExtractJadwal->created_at_rfid_tmp);
        $arrClean['id']= $ht45XExtractJadwal->id_ht45_rfid_tmp;
        $arrClean['nip']=$ht45XExtractJadwal->nip;
        $arrClean['rfid_tid']=$ht45XExtractJadwal->rfid_tid;
        $arrClean['rfid_reader_ip']=$ht45XExtractJadwal->rfid_reader_ip;
        $arrClean['rfid_reader_mac']=$ht45XExtractJadwal->rfid_reader_mac;

        $arrParse['REGNO']            = $ht45XExtractJadwal->nip;
        $arrParse['ABSDATE']          = $ht45XExtractJadwal->tanggal_absen;
        $arrParse['datetimedetected'] = $ht45XExtractJadwal->created_at_rfid_tmp;
        
        $arrParse['kodeinout']        = $ht45XExtractJadwal->status_pulang_masuk;
        $arrParse['nomesin']          = 999;
            
        if($ht45XExtractJadwal->status_pulang_masuk==0){
            $arrParse["kolom"]            = 'timein';
            if(is_null($ht45XExtractJadwal->date_time_detected)){
                $insertRfidClean=$this->insertClean($arrClean,'masuk',$ht45XExtractJadwal->created_at_rfid_tmp,NULL,$ht45XExtractJadwal->tanggal_absen);
                $inputHt08Prd = $this->injectHt08Prd($arrParse);
                $inputHt45Prd = $this->injectHt45Prd($arrParse);
                $ht08         = $this->updateHt08Lokal($ht45XExtractJadwal->tanggal_absen,$ht45XExtractJadwal->nip, 'timein', $ht45XExtractJadwal->created_at_rfid_tmp, $ht45XExtractJadwal->id_ht45_rfid_tmp);
                $insert_ht45 = $this->insertHt45Lokal($ht45XExtractJadwal->id_ht45_rfid_tmp, 1, $ht45XExtractJadwal->nip, '0', $ht45XExtractJadwal->created_at_rfid_tmp);
                // ExtractJadwalPegawai::where('id',$ht45XExtractJadwal->id_extract_jadwal)
                // ->update(['date_time_detected'=> $ht45XExtractJadwal->created_at_rfid_tmp ]);
                

            }
            else{
                $dateTimeDetected= new DateTime($ht45XExtractJadwal->date_time_detected);
                if($dateTimeRfid < $dateTimeDetected){
                    $insertRfidClean=$this->insertClean($arrClean,'masuk',$ht45XExtractJadwal->created_at_rfid_tmp,NULL,$ht45XExtractJadwal->tanggal_absen);
                    $inputHt08Prd = $this->injectHt08Prd($arrParse);
                    $inputHt45Prd = $this->injectHt45Prd($arrParse);
                    $ht08         = $this->updateHt08Lokal($ht45XExtractJadwal->tanggal_absen,$ht45XExtractJadwal->nip, 'timein', $ht45XExtractJadwal->created_at_rfid_tmp, $ht45XExtractJadwal->id_ht45_rfid_tmp);
                    $insert_ht45  = $this->insertHt45Lokal($ht45XExtractJadwal->id_ht45_rfid_tmp, 1, $ht45XExtractJadwal->nip, '0', $ht45XExtractJadwal->created_at_rfid_tmp);
                    // ExtractJadwalPegawai::where('id',$ht45XExtractJadwal->id_extract_jadwal)
                    // ->update(['date_time_detected'=> $ht45XExtractJadwal->created_at_rfid_tmp ]);
                    
                }
                else{
                    $ht45XExtractJadwal->flag_upload=2;
                    $ht45XExtractJadwal->save();
                    $return->reponse_code=400;
                    $return->data=$ht45XExtractJadwal;
                    return response()->json($return);
                }

            }
        }
        elseif($ht45XExtractJadwal->status_pulang_masuk==1){
            $arrParse["kolom"]            = 'timeout';
            if(is_null($ht45XExtractJadwal->date_time_detected)){
                $insertRfidClean=$this->insertClean($arrClean,'pulang',NULL,$ht45XExtractJadwal->created_at_rfid_tmp,$ht45XExtractJadwal->tanggal_absen);
                $inputHt08Prd = $this->injectHt08Prd($arrParse);
                $inputHt45Prd = $this->injectHt45Prd($arrParse);
                $ht08         = $this->updateHt08Lokal($ht45XExtractJadwal->tanggal_absen,$ht45XExtractJadwal->nip, 'timeout', $ht45XExtractJadwal->created_at_rfid_tmp, $ht45XExtractJadwal->id_ht45_rfid_tmp);
                $insert_ht45 = $this->insertHt45Lokal($ht45XExtractJadwal->id_ht45_rfid_tmp, 1, $ht45XExtractJadwal->nip, '1', $ht45XExtractJadwal->created_at_rfid_tmp);
                // ExtractJadwalPegawai::where('id',$ht45XExtractJadwal->id_extract_jadwal)
                // ->update(['date_time_detected'=> $ht45XExtractJadwal->created_at_rfid_tmp ]);
            }
            else{
                $dateTimeDetected= new DateTime($ht45XExtractJadwal->date_time_detected);
                if($dateTimeRfid > $dateTimeDetected){
                    $insertRfidClean=$this->insertClean($arrClean,'pulang',NULL,$ht45XExtractJadwal->created_at_rfid_tmp,$ht45XExtractJadwal->tanggal_absen);
                    $inputHt08Prd = $this->injectHt08Prd($arrParse);
                    $inputHt45Prd = $this->injectHt45Prd($arrParse);
                    
                    $ht08         = $this->updateHt08Lokal($ht45XExtractJadwal->tanggal_absen,$ht45XExtractJadwal->nip, 'timeout', $ht45XExtractJadwal->created_at_rfid_tmp, $ht45XExtractJadwal->id_ht45_rfid_tmp);
                    $insert_ht45 = $this->insertHt45Lokal($ht45XExtractJadwal->id_ht45_rfid_tmp, 1, $ht45XExtractJadwal->nip, '1', $ht45XExtractJadwal->created_at_rfid_tmp);
                    // ExtractJadwalPegawai::where('id',$ht45XExtractJadwal->id_extract_jadwal)
                    // ->update(['date_time_detected'=> $ht45XExtractJadwal->created_at_rfid_tmp ]);
                    
                }
                else{
                    $ht45XExtractJadwal->flag_upload=2;
                    $ht45XExtractJadwal->save();
                    $return->reponse_code=401;
                    $return->data=$ht45XExtractJadwal;
                    return response()->json($return);
                    
                }
                
            }
        }
       

        $ht45XExtractJadwal->flag_upload=1;
        $ht45XExtractJadwal->save();
        $return->reponse_code=200;
        $return->data=$ht45XExtractJadwal;
        return response()->json($return);
    }

    public function handleJadwalStatusStringKosong(){
        HT08KESHC::where('STATUS','')->update([
            'STATUS'=>NULL,
        ]);
        HT08MirroringCron::where('STATUS','')->update([
            'STATUS'=>NULL,
        ]);
        ExtractJadwalPegawai::where('status_ht08','')->update([
            'status_ht08'=>NULL,
        ]);   
    }
}
