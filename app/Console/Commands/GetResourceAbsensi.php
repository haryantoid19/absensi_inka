<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use DateTime;
use Carbon\Carbon;
use GuzzleHttp\Psr7\Request as GuzzleRequest;
use GuzzleHttp\Client;
use DB;
use Log;
use App\User;
use App\Rfid;
use App\RfidTmp;
use App\Ht01f;
use App\RangebyschHt04;
use App\RangeBySchSetting;
use stdClass;
use App\HT08MirroringCron;
use DateInterval;

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;


use App\HT08KESHC;

class GetResourceAbsensi extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'get:resource_absen';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get Ht08,Ht04,Ht04 settings,Ht01f';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->client=new Client();
        $this->interval_weekly=new DateInterval('P7D');
        $this->url= env('EOFFICE_PRD_URL', 'localhost').'api/v1/';
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
        date_default_timezone_set("Asia/Bangkok");
        $result = false;
        
        //get ht01f
        // while($result == false){
            $nowStart = new DateTime();
            $result = $this->getHt01fFromPrd();
            $result = $this->getRangeBySchHt04FromPrd();
            $result = $this->getRangeBySchHt04SettingFromPrd();
            $result = $this->cleansingHt08KeshcStatusInactive();
            //$result = $this->getHt08MirroringFromPrd();
            $nowStop = new DateTime();   
            $timeProcessing = $nowStart->diff($nowStop);
            $printOut = $timeProcessing->format('%hh:%im:%ss');

            // if($result != false) :
            //     echo 'Success getting HT01F, Ht04 at '.$nowStop->format('d-m-Y H:i:s').' | running time: '.$printOut." \n";
            // else :
            //     echo 'Error getting HT01F, Ht04 at '.$nowStop->format('d-m-Y H:i:s').' | running time: '.$printOut;
            //     sleep(1800);
            // endif;

            
        //}
       

        
    }
    public function getHt01f(){
        $now = new Carbon();
        $year = $now->format('Y');
        $month = $now->format('n');
        $url=env('API_KESHC_URL', 'localhost').'api/inka/ht01f';
        $client= new Client();

        if($month + 1 > 12){
            $nextMonth = 1;
            $nextYear = $year + 1;
        }
        else{
            $nextMonth = $month + 1;
            $nextYear = $year;
        }

        if($month - 1 < 1){
            $lastMonth = 12;
            $lastYear = $year - 1;
        }
        else{
            $lastMonth = $month - 1;
            $lastYear = $year;
        }

        try{
            //data bulan ini
            $formParams=[
                'username' => 'apikeshcinka_2018',
                'password' => 'secretapikeshcinka2018',
                'tahun' => $year,
                'bulan' => $month,
            ];
            $thisMonth = $client->request('POST', $url, array('form_params' => $formParams));
            $thisMonth =  json_decode($thisMonth->getBody()->getContents(), true);

            //data bulan lalu
            $formParams=[
                'username' => 'apikeshcinka_2018',
                'password' => 'secretapikeshcinka2018',
                'tahun' => $lastYear,
                'bulan' => $lastMonth,
            ];
            $lastMonth = $client->request('POST', $url, array('form_params' => $formParams));
            $lastMonth =  json_decode($lastMonth->getBody()->getContents(), true);

            //data bulan depan
            $formParams=[
                'username' => 'apikeshcinka_2018',
                'password' => 'secretapikeshcinka2018',
                'tahun' => $nextYear,
                'bulan' => $nextMonth,
            ];
            $nextMonth = $client->request('POST', $url, array('form_params' => $formParams));
            $nextMonth =  json_decode($nextMonth->getBody()->getContents(), true);

            
            DB::table('ht01f_mirror')->delete();
            DB::table('ht01f_mirror')->insert($thisMonth);
            DB::table('ht01f_mirror')->insert($lastMonth);
            DB::table('ht01f_mirror')->insert($nextMonth);
            DB::table('ht01f_mirror')->update([
                'created_at'=>$now->format('Y-m-d H:i:s'),

            ]);

            return $thisMonth;
        } catch(\Exception $e){
            Log::critical($e->getMessage());
            echo $e->getMessage();
            return false;
        }
    }


    private function getHt04(){
        $url=env('API_KESHC_URL', 'localhost').'api/inka/ht04';
        $client= new Client();
        $formParams=[
            'username' => 'apikeshcinka_2018',
            'password' => 'secretapikeshcinka2018'
        ];
        try{
            $data = $client->request('POST', $url, array('form_params' => $formParams));
            $data =  json_decode($data->getBody()->getContents());
            //return $data;
        } catch(\Exception $e){
            Log::critical($e->getMessage());
            return false;
        }
        DB::table('rangebysch_ht04')->delete();

        foreach ($data as $schedule ){
            //mengambil pengaturan jadwal jika belum ada, buat entry baru dengan data default 4, 2, 0, 5
            $rangeSetting = DB::table('rangebysch_settings')
            ->where('schtype', $schedule->SCHTYPE)
            ->where('shiftcode', $schedule->SHIFTCODE)
            ->first();
            if($rangeSetting == null){
                $rangeSetting = [
                    'schtype' => str_replace(' ', '', $schedule->SCHTYPE),
                    'shiftcode' => str_replace(' ', '', $schedule->SHIFTCODE),
                    'in_allowed_before' => 4,
                    'in_allowed_after' => 2,
                    'out_allowed_before' => 0,
                    'out_allowed_after' => 5,
                    'created_at' => Carbon::now(),
                ];
                DB::table('rangebysch_settings')
                ->insert($rangeSetting);
                $rangeSetting = (object) $rangeSetting;
            }

            //variabel range waktu masuk
            $inTime = new Carbon($schedule->TIMEFROM);
            $inTimeStart = $inTime->copy()->subMinutes($rangeSetting->in_allowed_before * 60);
            $inTimeEnd = $inTime->copy()->addMinutes($rangeSetting->in_allowed_after* 60)->subSecond();

            //variabel range waktu pulang
            $outTime = new Carbon($schedule->TIMETO);
            $outTimeStart = $outTime->copy()->subMinutes($rangeSetting->out_allowed_before * 60);
            $outTimeEnd = $outTime->copy()->addMinutes($rangeSetting->out_allowed_after * 60)->subSecond();

            //variabel range waktu normal
            $normalTimes = [
                [$inTimeEnd->copy()->addSecond(), $outTimeStart->copy()->subSecond()],
                [$outTimeEnd->copy()->addSecond(), $inTimeStart->copy()->subSecond()]
            ];


            $this->insertToRangeBySch($schedule, $inTimeStart, $inTimeEnd, 1); //insert range masuk
            $this->insertToRangeBySch($schedule, $outTimeStart, $outTimeEnd, 2); //insert range pulang
            //insert range normal 
            foreach($normalTimes as $normalTime){
                $this->insertToRangeBySch($schedule, $normalTime[0], $normalTime[1], 0, true);
            }
        }
        $lastWeekDate=new DateTime();
        $lastWeekDate=$lastWeekDate->sub($this->interval_weekly);
        $nextWeekDate=new DateTime();
        $nextWeekDate=$nextWeekDate->add($this->interval_weekly);
        $arrQuery['start_date']=$lastWeekDate->format('Y-m-d');
        $arrQuery['end_date']=$nextWeekDate->format('Y-m-d');
        $arrParse=[
            'schtype' =>'S1',
            'shiftcode' =>0,
            'range_start' =>'04:00:00',
            'range_end' =>'08:59:59',
            'absen' =>2,
            'kemaren' =>1,
            'created_at' =>Carbon::now(),
        ];
        $this->insertToRangeBySchCustom($arrParse);
    }
    
    /**
     * Insert range to rangebysch_ht04
     * 
     * @param stdObject schedule
     * @param Carbon start
     * @param Carbon end 
     * @param mixed status 
     * @param bool isNormalRange | apakah range yang dimasukkan merupakan range normal
     * 
     */
    private function insertToRangeBySch($schedule, $start, $end, $status, $isNormalRange=false)
    {
        if($start->format('H:i:s') > $end->format('H:i:s')){ //apabila range melewati hari
            DB::table('rangebysch_ht04')->insert([
                [
                    'schtype' => str_replace(' ', '', $schedule->SCHTYPE),
                    'shiftcode' => str_replace(' ', '', $schedule->SHIFTCODE),
                    'range_start' => $start->format('H:i:s'),
                    'range_end' => '23:59:59',
                    'absen' => $status,
                    'kemaren' => 0,
                    'created_at' => Carbon::now()
                ],
                [
                    'schtype' => str_replace(' ', '', $schedule->SCHTYPE),
                    'shiftcode' => str_replace(' ', '', $schedule->SHIFTCODE),
                    'range_start' => '00:00:00',
                    'range_end' => $end->format('H:i:s'),
                    'absen' => $status,
                    'kemaren' => ($isNormalRange) ? 0 : 1,
                    'created_at' => Carbon::now()
                ]
            ]);
        }
        else{
            
            DB::table('rangebysch_ht04')->insert([
                'schtype' => str_replace(' ', '', $schedule->SCHTYPE),
                'shiftcode' => str_replace(' ', '', $schedule->SHIFTCODE),
                'range_start' => $start->format('H:i:s'),
                'range_end' => $end->format('H:i:s'),
                'absen' => $status,
                'kemaren' => ($schedule->OVERDAY == 'Y' && $status == 2) ? 1 : 0,
                'created_at' => Carbon::now()
            ]);
        }
    }
    public function insertToRangeBySchCustom($arrParse=[]){
        DB::table('rangebysch_ht04')->insert($arrParse);
        return $arrParse;
    }

    public function getHT08Periodik(){
        echo "start download jadwal";
        $nowServer = date("Y-m-d", strtotime("-1 week"));
        $tomorrowServer = date("Y-m-d", strtotime("+2 week"));
        // $nowServer = "2018-11-26";
        // $tomorrowServer = "2018-11-30";
        $url=env('API_KESHC_URL', 'localhost').'api/inka/unduh-ht08-periode';
        $client= new Client();
        $formParams=[
            'username' => 'apikeshcinka_2018',
            'password' => 'secretapikeshcinka2018',
            'date1' => $tomorrowServer,
            'date2' => $nowServer,
        ];
        echo "start get jadwal";
        try{
            $data2 = $client->request('POST', $url, array('form_params' => $formParams));
            $data =  json_decode($data2->getBody()->getContents());
            // HT08MirroringCron::query()->delete();
            
            foreach ($data as $key => $value) {
                print_r($value);
                $value->REGNO = str_replace(' ', '', $value->REGNO);

                HT08MirroringCron::updateOrCreate(
                [ 
                   "REGNO"   =>$value->REGNO,
                   "ABSDATE" =>$value->ABSDATE
                ],
                [
                    "SCHTYPE" =>$value->SCHTYPE,
                    "WORKGRPCODE" =>$value->WORKGRPCODE,
                   
                    "PERIODE" =>$value->PERIODE,
                    "BULAN" =>$value->BULAN,
                    "SUBNO" =>$value->SUBNO,
                    "ATTDSTAT" =>$value->ATTDSTAT,
                    "PERMITNO" =>$value->PERMITNO,
                    "OVERTIMENO" =>$value->OVERTIMENO,
                    "FURLOUGHCODE" =>$value->FURLOUGHCODE,
                    "FPERIODE" =>$value->FPERIODE,
                    "FURLOUGHNO" =>$value->FURLOUGHNO,
                    "DATEIN" =>$value->DATEIN,
                    "DATEOUT" =>$value->DATEOUT,
                    "TIMEIN" =>$value->TIMEIN,
                    "TIMEOUT" =>$value->TIMEOUT,
                    "STATUS" =>$value->STATUS,
                    "REMARK" =>$value->REMARK,
                    "SPECIALSCH" =>$value->SPECIALSCH,
                    "SPDNO" =>$value->SPDNO,
                    "SCHIN" =>$value->SCHIN,
                    "SCHOUT" =>$value->SCHOUT,
                    "OVERHOUR" =>$value->OVERHOUR,
                    "OVERCOUNT" =>$value->OVERCOUNT,
                    "PERMITST" =>$value->PERMITST,
                    "JAHIL" =>$value->JAHIL,
                    "JALEB" =>$value->JALEB,
                    "MEHIL" =>$value->MEHIL,
                    "ABCD" =>$value->ABCD,
                ]);
                HT08KESHC::updateOrCreate(
                    [ 
                       "REGNO"   =>$value->REGNO,
                       "ABSDATE" =>$value->ABSDATE
                    ],
                    [
                        "SCHTYPE" =>$value->SCHTYPE,
                        "WORKGRPCODE" =>$value->WORKGRPCODE,
                       
                        "PERIODE" =>$value->PERIODE,
                        "BULAN" =>$value->BULAN,
                        "SUBNO" =>$value->SUBNO,
                        "ATTDSTAT" =>$value->ATTDSTAT,
                        "PERMITNO" =>$value->PERMITNO,
                        "OVERTIMENO" =>$value->OVERTIMENO,
                        "FURLOUGHCODE" =>$value->FURLOUGHCODE,
                        "FPERIODE" =>$value->FPERIODE,
                        "FURLOUGHNO" =>$value->FURLOUGHNO,
                        "DATEIN" =>$value->DATEIN,
                        "DATEOUT" =>$value->DATEOUT,
                        
                        "STATUS" =>$value->STATUS,
                        "REMARK" =>$value->REMARK,
                        "SPECIALSCH" =>$value->SPECIALSCH,
                        "SPDNO" =>$value->SPDNO,
                        "SCHIN" =>$value->SCHIN,
                        "SCHOUT" =>$value->SCHOUT,
                        "OVERHOUR" =>$value->OVERHOUR,
                        "OVERCOUNT" =>$value->OVERCOUNT,
                        "PERMITST" =>$value->PERMITST,
                        "JAHIL" =>$value->JAHIL,
                        "JALEB" =>$value->JALEB,
                        "MEHIL" =>$value->MEHIL,
                        "ABCD" =>$value->ABCD,
                    ]);
            }
        } catch(\Exception $e){
            Log::critical($e->getMessage());
            return false;
        }
        return true;
    }
    public function getRangeBySchHt04FromPrd()
    {
        $formParams=[
            
          
        ];
        print_r($formParams);
        $url=$this->url.'resource-absensi-get-ht04';
        try{
            $getDataJson = $this->client->request('POST', $url, array('form_params' => $formParams));
           
        } catch (\Exception $e){
            return false;
        }
        $getData     =  json_decode($getDataJson->getBody()->getContents(), true);
        
        $dataHt04 =  $getData['data'];
        
        RangebyschHt04::query()->delete();
        foreach ($dataHt04 as $key => $data) {
           $arrCreate=[
                'schtype'=>$data['schtype'],
                'shiftcode'=>$data['shiftcode'],
                'range_start'=>$data['range_start'],
                'range_end'=>$data['range_end'],
                'absen'=>$data['absen'],
                'kemaren'=>$data['kemaren'],
           ];
           $create=RangebyschHt04::create($arrCreate);
        }
        
        return true; 

    }

    public function getRangeBySchHt04SettingFromPrd()
    {
        $formParams=[
            
          
        ];
        print_r($formParams);
        $url=$this->url.'resource-absensi-get-rangebysch-setting';
        try{
            $getDataJson = $this->client->request('POST', $url, array('form_params' => $formParams));
           
        } catch (\Exception $e){
            return false;
        }
        $getData     =  json_decode($getDataJson->getBody()->getContents(), true);
        
        $dataHt0Settings =  $getData['data'];
        
        RangeBySchSetting::query()->delete();
        foreach ($dataHt0Settings as $key => $data) {
           $arrCreate=[
                'schtype' =>$data['schtype'],
                'shiftcode' => $data['shiftcode'],
                'in_allowed_before' => $data['in_allowed_before'],
                'in_allowed_after' =>$data['in_allowed_after'],
                'out_allowed_before'=>$data['out_allowed_before'],
                'out_allowed_after' =>$data['out_allowed_after'],
           ];
           $create=RangeBySchSetting::create($arrCreate);
           print_r($create);
        }
        
        return true; 
    }
    public function getHt01fFromPrd()
    {
        $formParams=[
            
          
        ];
        print_r($formParams);
        $url=$this->url.'resource-absensi-get-ht01f';
        try{
            $getDataJson = $this->client->request('POST', $url, array('form_params' => $formParams));
           
        } catch (\Exception $e){
            return false;
        }
        $getData     =  json_decode($getDataJson->getBody()->getContents(), true);
        
        $dataHt01f =  $getData['data'];
        
        Ht01f::query()->delete();
        foreach ($dataHt01f as $key => $data) {
           $arrCreate=[
            'SCHTYPE'=>$data['SCHTYPE'],
            'REGNO'=>$data['REGNO'],
            'PERIODE'=>$data['PERIODE'],
            'BULAN'=>$data['BULAN'],
            'DAY1'=>$data['DAY1'],
            'DAY2'=>$data['DAY2'],
            'DAY3'=>$data['DAY3'],
            'DAY4'=>$data['DAY4'],
            'DAY5'=>$data['DAY5'],
            
            'DAY6'=>$data['DAY6'],
            'DAY7'=>$data['DAY7'],
            'DAY8'=>$data['DAY8'],
            'DAY9'=>$data['DAY9'],
            'DAY10'=>$data['DAY10'],
            'DAY11'=>$data['DAY11'],
            'DAY12'=>$data['DAY12'],
            'DAY13'=>$data['DAY13'],
            'DAY14'=>$data['DAY14'],
            'DAY15'=>$data['DAY15'],
            'DAY16'=>$data['DAY16'],
            'DAY17'=>$data['DAY17'],
            'DAY18'=>$data['DAY18'],
            'DAY19'=>$data['DAY19'],
            'DAY20'=>$data['DAY20'],
            'DAY21'=>$data['DAY21'],
            'DAY22'=>$data['DAY22'],
            'DAY23'=>$data['DAY23'],
            'DAY24'=>$data['DAY24'],
            'DAY25'=>$data['DAY25'],
            'DAY26'=>$data['DAY26'],
            'DAY27'=>$data['DAY27'],
            'DAY28'=>$data['DAY28'],
            'DAY29'=>$data['DAY29'],
            'DAY30'=>$data['DAY30'],
            'DAY31'=>$data['DAY31'],
            'CREATEBY'=>$data['CREATEBY'],
            'CREATEDT'=>$this->validateDate($data['CREATEDT'])==true ? $data['CREATEDT'] : NULL,
            'MODIFYBY'=>$data['MODIFYBY'],
            'MODIFYDT'=>$this->validateDate($data['MODIFYDT'])==true ? $data['MODIFYDT'] : NULL,
           ];
           $create=Ht01f::create($arrCreate);
        }
        
        return true;   
    }
    public function getHt08MirroringFromPrd($arrParse=[]){
        $fromInterval= new DateInterval('P20D');
        $toInterval= new DateInterval('P10D');
        $startNow=new DateTime();
        $dateTimeStart= $startNow->sub($fromInterval);
        $endNow=new DateTime();
        $dateTimeEnd=$endNow->add($toInterval);
        $formParams=[
            'from'=>isset($arrParse['from']) ? $arrParse['from'] : $dateTimeStart->format('Y-m-d'),
            'to'=>isset($arrParse['to']) ? $arrParse['to'] : $dateTimeEnd->format('Y-m-d'),
        ];
        print_r($formParams);
        $url=$this->url.'resource-absensi-get-ht08';
        $apiRequest=new GuzzleRequest('POST', $url);
        try{
            $getDataJson = $this->client->request('POST', $url, ['form_params' => $formParams]);
        //dd($getDataJson);   
        } catch (\Exception $e){
            return false;
        }
        $getData     =  json_decode($getDataJson->getBody()->getContents(), true);
        
        $dataHt08    =  $getData['data'];
        
        foreach ($dataHt08 as $key => $value) {
            print_r($value);
            $arrCheck=[ 
                "REGNO"   =>$value['REGNO'],
                "ABSDATE" =>$value['ABSDATE'],
            ];
            $arrUpdate=[
                "SCHTYPE" =>$value['SCHTYPE'],
                "WORKGRPCODE" =>$value['WORKGRPCODE'],
                
                "PERIODE" =>$value['PERIODE'],
                "BULAN" =>$value['BULAN'],
                "SUBNO" =>$value['SUBNO'],
                "ATTDSTAT" =>$value['ATTDSTAT'],
                "PERMITNO" =>$value['PERMITNO'],
                "OVERTIMENO" =>$value['OVERTIMENO'],
                "FURLOUGHCODE" =>$value['FURLOUGHCODE'],
                "FPERIODE" =>$value['FPERIODE'],
                "FURLOUGHNO" =>$value['FURLOUGHNO'],
                "DATEIN" =>$value['DATEIN'],
                "DATEOUT" =>$value['DATEOUT'],
                
                "STATUS" =>$value['STATUS'],
                "REMARK" =>$value['REMARK'],
                "SPECIALSCH" =>$value['SPECIALSCH'],
                "SPDNO" =>$value['SPDNO'],
                "SCHIN" =>$value['SCHIN'],
                "SCHOUT" =>$value['SCHOUT'],
                "OVERHOUR" =>$value['OVERHOUR'],
                "OVERCOUNT" =>$value['VERCOUNT'],
                "PERMITST" =>$value['PERMITST'],
                "JAHIL" =>$value['JAHIL'],
                "JALEB" =>$value['JALEB'],
                "MEHIL" =>$value['MEHIL'],
                "ABCD" =>$value['ABCD'],
            ];
            HT08KESHC::updateOrCreate($arrCheck,$arrUpdate);
            $arrUpdate["TIMEIN"]=$value['TIMEIN'];
            $arrUpdate["TIMEOUT"]=$value['TIMEOUT'];
            HT08MirroringCron::updateOrCreate($arrCheck,$arrUpdate);
            
        }
        
        return true;   
    }
    public function cleansingHt08KeshcStatusInactive(){
        HT08KESHC::whereIn('STATUS',['L','R','B','C','S','I','K','D'])
        ->whereNotNull('TIMEIN')->update(['TIMEIN'=>NULL]);
        HT08KESHC::whereIn('STATUS',['L','R','B','C','S','I','K','D'])
        ->whereNotNull('TIMEOUT')->update(['TIMEOUT'=>NULL]);
    }
    public function validateDate($date, $format = 'Y-m-d')
    {
        $d = DateTime::createFromFormat($format, $date);
        // The Y ( 4 digits year ) returns TRUE for any integer with any number of digits so changing the comparison from == to === fixes the issue.
        return $d && $d->format($format) === $date;
    }

}
