<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HT08KESHC extends Model
{
    //
    protected $table = 'ht08_keshc';
    protected $guarded=[];
    
    public $timestamps = true;

    public function user()
	{
  
        return $this->hasOne('App\User', 'nip', 'REGNO')->withTrashed();
	}
}
