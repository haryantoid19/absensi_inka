<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MasterRfidReader extends Model
{
    //
    protected $table = 'master_rfid_reader';
    
    protected $fillable = ['rfid_reader_mac', 'nama','worker'];
    public $incrementing = false;

	public function rfid()
	{
		return $this->hasMany('App\Rfid', 'rfid_reader_mac', 'rfid_reader_mac');
	}
}
