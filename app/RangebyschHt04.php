<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RangebyschHt04 extends Model
{
    //
    protected $table = 'rangebysch_ht04';
    public $timestamps = true;
    protected $guarded=[];
}
