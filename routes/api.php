<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::group(['prefix' => 'v1', 'namespace' => 'Api\V1'], function() {
    Route::post('inject-rfid-no-treshold','Absensi\AbsensiFingerController@injectKotorFromFingerNoTreshold')->name('inject-rfid.no-treshold');
    
    Route::group(['middleware' => 'api.auth'], function() {
        Route::group(['prefix' => 'rfid'],function(){
            Route::post('history-rfid','Absensi\RiwayatController@riwayat');
        });
        Route::group(['prefix' => 'data-clean'],function(){
            Route::post('index','Absensi\RfidCleanController@index');
        });
    });
});
Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

